# FLASH PP Pipeline

This pipeline is used to extract data and to create a synthetic observations using RADMC-3D in a parallized manner.

## Installation

Just copy over the pipeline and check that you have all necessary python packages installed.
More information can be found on the wiki pages on bitbucket.

## Usage

To use the pipeline you have to execute the run\_script.py with following arguments:

```bash
python run_script.py <path to data folder>  <path to scripts folder>  <path to settings file>
```

where the data folder has to contain the subfolders defined in the STRUCTURE section of the settings.txt file.

