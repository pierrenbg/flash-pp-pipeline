import sys
import os
import shutil
import collections

data_folder = os.path.abspath(sys.argv[1])
script_folder = os.path.abspath(sys.argv[2])
settings_file = os.path.abspath(sys.argv[3])
home_folder = os.getcwd()

# This imports the scripts.py file from the script folder by adding the script folder to the system path.
sys.path.insert(0, script_folder)
import scripts


# Read the settings file and return the wavelength, lines, radmc, structure, orientations settings as well as
# the different settings for the data manipulation and the information for the radmc call.
def read_settings(fname):
	with open(fname, 'r') as f:
		data = f.read()
	data = data.split('\n')

	wavelength_start = data.index('WAVELENGTHSETTINGS')
	wavelength_end = data.index('WAVELENGTHSETTINGSEND')

	lines_start = data.index('LINESSETTINGS')
	lines_end = data.index('LINESSETTINGSEND')

	radmc_start = data.index('RADMC3DCALL')
	radmc_end = data.index('RADMC3DCALLEND')

	radmcset_start = data.index('RADMC3DSET')
	radmcset_end = data.index('RADMC3DSETEND')

	structure_start = data.index('STRUCTURE')
	structure_end = data.index('STRUCTUREEND')

	orientations_start = data.index('ORIENTATIONS')
	orientations_end = data.index('ORIENTATIONSEND')

	flags_start = data.index('FLAGS')
	flags_end = data.index('FLAGSEND')

	lines_set = data[lines_start + 1: lines_end]
	radmc_set = data[radmcset_start + 1: radmcset_end]
	radmc_call = data[radmc_start + 1: radmc_end]
	folder_list = data[structure_start + 1: structure_end]
	orientations = data[orientations_start + 1: orientations_end]
	orientations = [orientations[i].replace(' ', '_') for i in range(len(orientations))]
	wavelength_set = data[wavelength_start + 1: wavelength_end]
	flags_set = data[flags_start + 1: flags_end]

	wavelength_dict = {
		'lam_0'		:	0,
		'nlam'		:	0,
		'widthkms'	:	0
	}

	for row in wavelength_set:
		tmp_key, tmp_val = row.split()
		wavelength_dict[tmp_key] = eval(tmp_val)

	flags_dict = build_dict(flags_set)
	return wavelength_dict, flags_dict, orientations, folder_list, lines_set, radmc_set, radmc_call


# Reads the dictionary from a file to maybe rerun a simulation.
# Currently not implemented or used.
def read_dict(fname):
	with open(fname) as f:
		dictionary = dict()
		for line in f.readlines():
			name, value = line.rstrip('\n').split(':')
			dictionary[eval(name)] = eval(value)
	return dictionary


# Returns a dictionary with all given settings.
def build_dict(settings):
	set_dict = collections.OrderedDict(
		[
			('mol_type', None),
			('freezeout', 0),
			('wf', 0),
			('spin_temp', 0),
			('threshold', 20000.),
			('chemnet', None),
			('crir', 3.e-17),
			('dust_to_gas', 0.01),
			('mol_mass', 2.3),
			('coords', None),
			('dtype', None),
			('mode', 'parallel'),
			('debug', False),
			('outputname', 3),
			('reduc', False),
			('ftype', 'ascii'),
			('cuboid', False)
		]
	)

	for i in range(len(settings)):
		tmp_set, tmp_val = settings[i].split()[0], "".join(settings[i].split()[1:])
		set_dict[tmp_set] = eval(tmp_val)

	return set_dict


# Creates a list of dictionaries from the main dictionary for all possible combinations.
# Some selection rules are already implemneted.
def dict_list(full_dict):
	keys = full_dict.keys()
	d_list = [full_dict]
	for key in keys:
		if isinstance(full_dict[key], list) and key != 'coords' and key != 'outputname':
			tmp = list()
			for d in d_list:
				for item in full_dict[key]:
					d_tmp = d.copy()
					counter = 0
					d_tmp[key] = item
					if d_tmp['spin_temp'] == 1 and d_tmp['mol_type'] != 'ha':
						d_tmp['spin_temp'] = 0
					if d_tmp['spin_temp'] == 0:
						d_tmp['wf'] = 0
					for j in range(len(d_list)):
						if d_tmp == d_list[j]:
							counter += 1
					for k in range(len(tmp)):
						if d_tmp == tmp[k]:
							counter += 1
					if counter == 0:
						tmp.append(d_tmp.copy())
			if tmp != list():
				d_list = tmp
	return d_list


# Write a file with all settings.
def write_settings(fpath, d_set):
	with open(os.path.join(fpath, 'settings.txt'), 'w+') as f:
		for tmp_key, tmp_val in d_set.items():
			if type(tmp_key) == str:
				tmp_key = "'%s'" % tmp_key
			if type(tmp_val) == str:
				tmp_val = "'%s'" % tmp_val
			f.write('%s : %s\n' % (tmp_key, tmp_val))


# Creates a list of all keys which have multiple values.
def get_dict_diff(full_dict):
	diff_list = []
	for key in full_dict.keys():
		if isinstance(full_dict[key], list) and full_dict[key] != []:
			if len(full_dict[key]) > 1:
				diff_list.append(key)
	return diff_list


# Creates the folder structure and also the settings files in each folder as well as a more general settings file in
# the main folder to know which folder contains which settings combination.
def create_folder_structure(structure, d_list, directions, full_dict):
	path_list = []
	keys = get_dict_diff(full_dict=full_dict)
	for path in structure:
		counter = 0
		f_set = open(os.path.join(data_folder, path, 'folder_settings.txt'), 'w+')
		tmp_files = \
			[f for f in os.listdir(os.path.join(data_folder, path)) if os.path.isfile(os.path.join(data_folder, path, f))]
		tmp_files.sort()
		for tf in tmp_files:
			if 'txt' in tf:
				tmp_files.pop(tmp_files.index(tf))
		for i in range(len(tmp_files)):
			for k in range(len(d_list)):
				f_set.write('folder_%s\t%s\t\t' % (counter, tmp_files[i]))
				for key in keys:
					f_set.write(str(key) + ' ')
					f_set.write(str(d_list[k][key]) + '\t')
				tmp_path = os.path.join(data_folder, path, 'folder_%s' % counter)
				if not os.path.exists(tmp_path):
					os.makedirs(tmp_path)
				path_list.append([tmp_path, os.path.join(data_folder, path, tmp_files[i])])
				write_settings(tmp_path, d_list[k])
				f_set.write('\n')
				counter += 1
				for j in range(len(directions)):
					if not os.path.exists(os.path.join(tmp_path, directions[j])):
						os.makedirs(os.path.join(tmp_path, directions[j]))
		f_set.close()
	return path_list


# Writes the bash files which make it possible to run all radmc simulations in parallel.
# This is accomplished by creating a script which calls the create_data function from the scripts.py file with the right
# settings combination. This same script also creates a file which will call the right radmc version with the all given
# call parameters which also creates all the fits files if we use the image call for radmc.
def write_radmc_parallel(
		dpath, id_nr, directions, radmc_path, radmc_call, wave, outputname, add_string=str(), rewrite=False
):

	if isinstance(radmc_call, list):
		radmc_call = ' '.join(radmc_call)
		# for i in range(len(radmc_call)):
		# 	tmp = tmp + radmc_call[i] + ' '
		# radmc_call = tmp[:-1]

	if not rewrite:
		with open(os.path.join(dpath, 'run_data.py'), 'w+') as py_file:
			py_file.write('import sys\n')
			py_file.write('import os\n')
			py_file.write('import shutil\n')
			py_file.write('sys.path.insert(0,"%s")\n' % script_folder)
			py_file.write('import scripts\n')
			py_file.write('\n')
			py_file.write('%s\n' % add_string)
			py_file.write('\n')
			py_file.write('directions = %s\n' % directions)
			py_file.write('opath = "%s"\n' % dpath)
			py_file.write('spath = "%s"\n' % script_folder)
			py_file.write('hpath = "%s"\n' % home_folder)
			py_file.write('rpath = "%s"\n' % radmc_path)
			py_file.write('radmc_call = "%s"\n' % radmc_call)
			py_file.write('id_nr = %d\n' % id_nr)
			py_file.write('wavelength = %s\n' % wave)
			py_file.write('outputname = %s\n' % outputname)
			py_file.write('flist = [f for f in os.listdir(opath) if os.path.isfile(os.path.join(opath, f))]\n')
			py_file.write('for i in range(len(directions)):\n')
			py_file.write('\tfor j in range(len(flist)):\n')
			py_file.write('\t\tif not os.path.islink(os.path.join(opath, directions[i], flist[j])):\n')
			py_file.write(
				'\t\t\tos.symlink(os.path.join(opath, flist[j]), os.path.join(opath, directions[i], flist[j]))\n')
			py_file.write(
				'\tshutil.copy2(os.path.join(spath, "run_radmc_parallel.sh"),'
				'os.path.join(hpath, "run_radmc_parallel_%s_%s.sh" % (id_nr, i)))\n')
			py_file.write('\n')
			py_file.write('\twith open(os.path.join(hpath, "run_radmc_parallel_%d_%d.sh" % (id_nr, i)), "a") as f:\n')
			py_file.write('\t\tf.write("cd %s \\n" % os.path.join(opath, directions[i]))\n')
			py_file.write('\t\tf.write("%s %s %s\\n" % (rpath, radmc_call, directions[i].replace("_", " ")))\n')
			py_file.write('\t\tf.write("\\n")\n')
			py_file.write('\t\tf.write("if [ -a %s ]\\n" % os.path.join(opath, directions[i], "image.out"))\n')
			py_file.write('\t\tf.write("\\tthen\\n")\n')
			py_file.write(
				'\t\tf.write("\\t\\tpython %s\\n" % os.path.join(spath, "out2fits.py %s \'%s\'" % (wavelength, '
				'outputname)))\n')
			py_file.write('\t\tf.write("fi")')

		shutil.copy2(
			os.path.join(script_folder, 'run_data_parallel.sh'),
			os.path.join(home_folder, 'run_data_parallel_%d.sh' % id_nr))

		with open(os.path.join(home_folder, 'run_data_parallel_%d.sh' % id_nr), 'a') as f:
			f.write('\n')
			f.write('python %s \n' % (os.path.join(dpath, 'run_data.py')))
	else:
		if isinstance(radmc_call, list):
			tmp = ''
			for i in range(len(radmc_call)):
				tmp = tmp + radmc_call[i] + ' '
			radmc_call = tmp[:-1]
		for i in range(len(directions)):
			shutil.copy2(
				os.path.join(script_folder, "run_radmc_parallel.sh"),
				os.path.join(home_folder, "run_radmc_parallel_%s_%s.sh" % (id_nr, i)))
			with open(os.path.join(home_folder, "run_radmc_parallel_%d_%d.sh" % (id_nr, i)), "a") as f:
				f.write("cd %s \n" % os.path.join(dpath, directions[i]))
				f.write("%s %s %s\n" % (radmc_path, radmc_call, directions[i].replace("_", " ")))
				f.write("\n")
				f.write("if [ -a %s ]\n" % os.path.join(dpath, directions[i], "image.out"))
				f.write("\tthen\n")
				f.write("\t\tpython %s\n" % os.path.join(script_folder, "out2fits.py %s '%s'" % (wave, outputname)))
				f.write("fi")


def rewrite_radmc_parallel(dpath, id_nr, directions, radmc_path, radmc_call, wave, outputname):
	if isinstance(radmc_call, list):
		tmp = ''
		for i in range(len(radmc_call)):
			tmp = tmp + radmc_call[i] + ' '
		radmc_call = tmp[:-1]
	for i in range(len(directions)):
		shutil.copy2(
			os.path.join(script_folder, "run_radmc_parallel.sh"),
			os.path.join(home_folder, "run_radmc_parallel_%s_%s.sh" % (id_nr, i)))
		with open(os.path.join(home_folder, "run_radmc_parallel_%d_%d.sh" % (id_nr, i)), "a") as f:
			f.write("cd %s \n" % os.path.join(dpath, directions[i]))
			f.write("%s %s %s\n" % (radmc_path, radmc_call, directions[i].replace("_", " ")))
			f.write("\n")
			f.write("if [ -a %s ]\n" % os.path.join(dpath, directions[i], "image.out"))
			f.write("\tthen\n")
			f.write("\t\tpython %s\n" % os.path.join(script_folder, "out2fits.py %s '%s'" % (wave, outputname)))
			f.write("fi")


# The main routine which creates the shell script which calls the the python routine to creates all data.
def write_data_sh(fpath, dpath, tmp_dict, radmc_set, lines_set, camera_set, id_nr, directions, radmc_path, radmc_call):
	output_str = \
		'scripts.create_data(fname="{fname}", dpath="{dpath}", script_folder="{script_folder}", ' \
		'dtype="{dtype}", mol_type="{mol_type}", chemnet="{chemnet}", radset={radset}, ' \
		'linesset={linesset}, cameraset={cameraset}, coords={coords}, threshold={threshold}, ' \
		'freezeout={freezeout}, crir={crir}, wf={wf}, dust_to_gas={dust_to_gas}, mol_mass={mol_mass},' \
		'debug={debug}, reduc={reduc}, ftype="{ftype}", cuboid={cuboid})'

	output_str = output_str.format(
		fname=fpath, dpath=dpath, script_folder=script_folder, dtype=tmp_dict['dtype'], mol_type=tmp_dict['mol_type'],
		chemnet=tmp_dict['chemnet'], radset=radmc_set, linesset=lines_set, cameraset=camera_set,
		coords=tmp_dict['coords'], threshold=tmp_dict['threshold'], freezeout=tmp_dict['freezeout'],
		crir=tmp_dict['crir'], wf=tmp_dict['wf'], dust_to_gas=tmp_dict['dust_to_gas'], mol_mass=tmp_dict['mol_mass'],
		debug=tmp_dict['debug'], reduc=tmp_dict['reduc'], ftype=tmp_dict['ftype'], cuboid=tmp_dict['cuboid']
	)

	# shutil.copy2(
	# 	os.path.join(script_folder, 'run_data_parallel.sh'),
	# 	os.path.join(home_folder, 'run_data_parallel_%d.sh' % id_nr))

	write_radmc_parallel(
		dpath=dpath, id_nr=id_nr, directions=directions, radmc_path=radmc_path, add_string=output_str,
		radmc_call=radmc_call, wave=camera_set['lam_0'], outputname=tmp_dict['outputname'])

	# with open(os.path.join(home_folder, 'run_data_parallel_%d.sh' % id_nr), 'a') as f:
	# 	f.write('\n')
	# 	f.write('python %s \n' % (os.path.join(dpath, 'run_data.py')))


# Creates the data creation routine and chooses the needed radmc version.
# This offers also the possibility to let this routine run in a serial way, so one does not need to call
# all the different bash scripts.
def create_data(
		path_list, d_list, radmc_set, lines_set, camera_set, directions, radmc_call, full_dict, rewriteinp=False):
	num_dict = len(d_list)
	for i in range(len(path_list)):
		tmp_dict = d_list[i % num_dict].copy()
		if tmp_dict['spin_temp'] == 1:
			radmc_path = os.path.join(script_folder, 'radmc', 'radmc3d_0.40', 'radmc3d')
			if not os.path.isfile(radmc_path):
				cpath = os.getcwd()
				os.chdir(os.path.join(script_folder, 'radmc', 'radmc3d_0.40'))
				os.system('make')
				os.chdir(cpath)
		elif tmp_dict['spin_temp'] == 0:
			radmc_path = os.path.join(script_folder, 'radmc', 'radmc3d_0.41', 'radmc3d')
			if not os.path.isfile(radmc_path):
				cpath = os.getcwd()
				os.chdir(os.path.join(script_folder, 'radmc', 'radmc3d_0.41'))
				os.system('make')
				os.chdir(cpath)
		else:
			sys.exit('Please set spin_temp to either 1 or 0 to in-/exclude spin temperatures.')

		tmp_index = full_dict['mol_type'].index(tmp_dict['mol_type'])
		tmp_camera_set = camera_set.copy()
		if isinstance(camera_set['lam_0'], list):
			if len(camera_set['lam_0']) > 1:
				tmp_camera_set['lam_0'] = tmp_camera_set['lam_0'][tmp_index]
			else:
				tmp_camera_set['lam_0'] = camera_set['lam_0'][0]
		else:
			tmp_camera_set = camera_set

		if isinstance(camera_set['nlam'], list):
			if len(camera_set['nlam']) > 1:
				tmp_camera_set['nlam'] = tmp_camera_set['nlam'][tmp_index]
			else:
				tmp_camera_set['nlam'] = camera_set['nlam'][0]
		else:
			tmp_camera_set = camera_set

		if isinstance(camera_set['widthkms'], list):
			if len(camera_set['widthkms']) > 1:
				tmp_camera_set['widthkms'] = tmp_camera_set['widthkms'][tmp_index]
			else:
				tmp_camera_set['widthkms'] = camera_set['widthkms'][0]
		else:
			tmp_camera_set = camera_set

		if full_dict['mode'] == 'serial' or rewriteinp:
			if not rewriteinp:
				scripts.create_data(
					fname=path_list[i][1], dpath=path_list[i][0], script_folder=script_folder,
					dtype=tmp_dict['dtype'], mol_type=tmp_dict['mol_type'], chemnet=tmp_dict['chemnet'],
					radset=radmc_set, linesset=lines_set, cameraset=tmp_camera_set, coords=tmp_dict['coords'],
					threshold=tmp_dict['threshold'], freezeout=tmp_dict['freezeout'], crir=tmp_dict['crir'],
					wf=tmp_dict['wf'], dust_to_gas=tmp_dict['dust_to_gas'], mol_mass=tmp_dict['mol_mass'],
					debug=tmp_dict['debug'], reduc=tmp_dict['reduc'], ftype=tmp_dict['ftype'],
					cuboid=tmp_dict['cuboid']
				)
			else:
				scripts.create_data(
					fname=path_list[i][1], dpath=path_list[i][0], script_folder=script_folder,
					dtype=tmp_dict['dtype'], mol_type=tmp_dict['mol_type'], chemnet=tmp_dict['chemnet'],
					radset=radmc_set, linesset=lines_set, cameraset=tmp_camera_set, coords=tmp_dict['coords'],
					threshold=tmp_dict['threshold'], freezeout=tmp_dict['freezeout'], crir=tmp_dict['crir'],
					wf=tmp_dict['wf'], dust_to_gas=tmp_dict['dust_to_gas'], mol_mass=tmp_dict['mol_mass'],
					debug=tmp_dict['debug'], rewrite=True, reduc=tmp_dict['reduc'], ftype=tmp_dict['ftype'],
					cuboid=tmp_dict['cuboid']
				)

			write_radmc_parallel(
				dpath=path_list[i][0], id_nr=i, directions=directions, radmc_path=radmc_path,
				radmc_call=radmc_call, outputname=tmp_dict['outputname'], wave=tmp_camera_set['lam_0'])

		elif full_dict['mode'] == 'parallel':
			write_data_sh(
				fpath=path_list[i][1], dpath=path_list[i][0], tmp_dict=tmp_dict, radmc_set=radmc_set,
				lines_set=lines_set, camera_set=tmp_camera_set, id_nr=i, directions=directions,
				radmc_path=radmc_path, radmc_call=radmc_call)


def read_arguments():
	import scipy.constants as const
	import numpy as np

	# We use the RADMC3D value here as it differs to much from the real definition of parsec.
	parsec = 3.08572e16

	args = sys.argv[:]
	arg_dict = {
		'mode'		:	0,
		'item'		:	0,
		'settings'	:	[]
	}

	wavelength_dict, flags_dict, directions, folder_list, lines_set, radmc_set, radmc_call = \
		read_settings(fname=settings_file)

	dlist = dict_list(full_dict=flags_dict)

	path_list = create_folder_structure(
		structure=folder_list, d_list=dlist, directions=directions, full_dict=flags_dict)

	if len(flags_dict['coords']) == 6 and flags_dict['dtype'] == 'flash':
		xl = np.asarray(flags_dict['coords'][0:3])
		xr = np.asarray(flags_dict['coords'][3:])
		if not any('point' in s for s in radmc_call):
			# Parsec changed to RADMC3D value
			center = (xl + xr)/(2. * parsec * 100)
			radmc_call.append('pointpc %.4f %.4f %.4f' % (center[0], center[1], center[2]))
		if not any('size' in s for s in radmc_call):
			# Parsec changed to RADMC3D value
			size = np.max((xr - xl)/(parsec * 100))
			radmc_call.append('sizepc %.4f' % size)

	if len(args) > 4:
		arg_dict['mode'] = args[4]

		if arg_dict['mode'] == 'radmc_parallel':
			num_dict = len(dlist)
			for i in range(len(path_list)):
				tmp_dict = dlist[i % num_dict].copy()
				tmp_index = flags_dict['mol_type'].index(tmp_dict['mol_type'])
				tmp_camera_set = wavelength_dict.copy()
				if isinstance(wavelength_dict['lam_0'], list):
					if len(wavelength_dict['lam_0']) > 1:
						tmp_camera_set['lam_0'] = tmp_camera_set['lam_0'][tmp_index]
					else:
						tmp_camera_set['lam_0'] = wavelength_dict['lam_0'][0]
				else:
					tmp_camera_set = wavelength_dict

				if isinstance(wavelength_dict['nlam'], list):
					if len(wavelength_dict['nlam']) > 1:
						tmp_camera_set['nlam'] = tmp_camera_set['nlam'][tmp_index]
					else:
						tmp_camera_set['nlam'] = wavelength_dict['nlam'][0]
				else:
					tmp_camera_set = wavelength_dict

				if isinstance(wavelength_dict['widthkms'], list):
					if len(wavelength_dict['widthkms']) > 1:
						tmp_camera_set['widthkms'] = tmp_camera_set['widthkms'][tmp_index]
					else:
						tmp_camera_set['widthkms'] = wavelength_dict['widthkms'][0]
				else:
					tmp_camera_set = wavelength_dict

				if tmp_dict['spin_temp'] == 1:
					radmc_path = os.path.join(script_folder, 'radmc', 'radmc3d_0.40', 'radmc3d')
					if not os.path.isfile(radmc_path):
						cpath = os.getcwd()
						os.chdir(os.path.join(script_folder, 'radmc', 'radmc3d_0.40'))
						os.system('make')
						os.chdir(cpath)
				elif tmp_dict['spin_temp'] == 0:
					radmc_path = os.path.join(script_folder, 'radmc', 'radmc3d_0.41', 'radmc3d')
					if not os.path.isfile(radmc_path):
						cpath = os.getcwd()
						os.chdir(os.path.join(script_folder, 'radmc', 'radmc3d_0.41'))
						os.system('make')
						os.chdir(cpath)
				else:
					sys.exit('Please set spin_temp to either 1 or 0 to in-/exclude spin temperatures.')

				write_radmc_parallel(
					dpath=path_list[i][0], id_nr=i, directions=directions, radmc_path=radmc_path,
					radmc_call=radmc_call, rewrite=True, outputname=tmp_dict['outputname'],
					wave=tmp_camera_set['lam_0'])

		elif arg_dict['mode'] == 'add_input':
			create_data(
				path_list=path_list, camera_set=wavelength_dict, d_list=dlist, directions=directions,
				lines_set=lines_set, radmc_set=radmc_set, radmc_call=radmc_call, full_dict=flags_dict,
				rewriteinp=True
			)

	else:
		create_data(
			path_list=path_list, camera_set=wavelength_dict, d_list=dlist, directions=directions,
			lines_set=lines_set, radmc_set=radmc_set, radmc_call=radmc_call, full_dict=flags_dict)


# Reads out the settings file, creates a dictionary and path list and afterwards calls the data creation routine.
def run():
	wavelength_dict, flags_dict, directions, folder_list, lines_set, radmc_set, radmc_call = \
		read_settings(fname=settings_file)

	dlist = dict_list(full_dict=flags_dict)

	path_list = create_folder_structure(
		structure=folder_list, d_list=dlist, directions=directions, full_dict=flags_dict)

	create_data(
		path_list=path_list, camera_set=wavelength_dict, d_list=dlist, directions=directions,
		lines_set=lines_set, radmc_set=radmc_set, radmc_call=radmc_call, full_dict=flags_dict)


read_arguments()

# Done:
# create data structure with all needed folders and directions
# read in all settings and created a list of directories with all settings combinations
# create data from files in rigth folders and create links in parallel or serial mode.
# create shell scripts for parallel radmc3d runs
# select right radmc version for different simulations
# rewritten out2fit to be more versatile
# include a fast way to write out box from simulation data without quickflash
#
# ToDo:
# refine data creation with necessary conditions
# find a way to give folders useful names corresponding to settings
# pass dictionary to scripts.create_data instead of all variables
# implement way of creating images for multiple wavelenghts at once
# Restart options (kind of done)
# Write out level population
#
