# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import numpy as np
import matplotlib.pylab as plt
import sys
import matplotlib.colors as cl
from scipy.spatial import cKDTree
from itertools import izip

__author__ = "Michael Weis"
__version__ = "0.0.4.0"

# ==== PHYSICS =================================================================
G_g = 6.67408e-8    # Gravitational constant (cgs)
M_sol = 1.9884e+33  # solar mass (g)
Myr = 3.15576e+13   # 1e+6 julian years (s)
pc = 3.0856776e+18     # parsec (cm)
k_b = 1.38064852e-16    # Boltzmann constant (erg/K)
m_a = 1.660539040e-24     # atomic mass unit (g)/(Da)
km = 1e+5

# ==== SETTINGS ================================================================
dist_min, dist_max = .05*pc, 3.*pc
dist_norm = cl.Normalize(dist_min, dist_max)
dist_res = 250
N_res = 50000

MaxTemp = np.inf #1000 #775.

# ==== HELPER ==================================================================    
def distance(A, B, D_cutoff):
    ATree = cKDTree(A)
    if A is B:
        I,J = izip(*ATree.query_pairs(D_cutoff))
        diff = A[np.array(I)]-B[np.array(J)]
        dist = np.sqrt((diff**2).sum(axis=-1))
    else:
        BTree = cKDTree(B)
        Mdist = ATree.sparse_distance_matrix(BTree, D_cutoff)
        dist = Mdist.ravel()
    return dist

def partition(norm, res):
    bounds_normed = np.linspace(0., 1., res+1)
    midpoints_normed = .5 * (bounds_normed[1:] +bounds_normed[:-1])
    bounds = np.asarray(norm.inverse(bounds_normed))
    support = np.asarray(norm.inverse(midpoints_normed))
    return bounds, support

def separation_hist(A, B, bounds):
    res = len(bounds)-1
    dist = distance(A, B, bounds[-1])
    dist_i = np.searchsorted(bounds, dist)
    dist_n = np.bincount(dist_i, minlength=res+2)
    return dist_n

def random_coords(count, extent):
    dim = len(extent)
    X_normalized = np.random.rand(count, dim)
    X_span = extent[:,1]-extent[:,0]
    X = extent[:,0] + X_span*X_normalized
    return X

# ==== MAIN FUNCTIONS ==========================================================    
def draw_point_sample(ffile, fgrid, probvar='mass', selector=None,
                      N_samples=50000, verbose=True):
    if verbose:
        print '-> Drawing point sample...'
        print '   Sample points: %i'%N_samples
        sys.stdout.flush()
    # Get blockcell coordinate list
    coords = fgrid.coords()
    # Apply selector:
    if selector is None:
        selector = np.full_like(coords[:,0].ravel(), True, dtype=bool)
    X = coords[:,0].ravel()[selector]
    Y = coords[:,1].ravel()[selector]
    Z = coords[:,2].ravel()[selector]
    coordlist = np.array((X,Y,Z)).T
    # Read the distribution var & apply selector
    cumvar = ffile[probvar].ravel()[selector]
    # Distribute point samples according to distribution var:
    mcs = np.cumsum(cumvar)
    ## Randomize blockcell ID according to mass distribution
    mcs_points = np.random.rand(N_samples) * mcs[-1]
    blockcell_index = np.searchsorted(mcs, mcs_points)
    ## Randomize inter blockcell position
    blocksize = ffile['block size']
    nxb = ffile['integer scalars/nxb']
    nyb = ffile['integer scalars/nyb']
    nzb = ffile['integer scalars/nzb']
    cellsize = blocksize / np.array([nxb, nyb, nzb])
    bcs = ((cellsize.T)[:,:,None,None,None]*np.ones([nxb, nyb, nzb]))
    blockcell_size = bcs.reshape(3,-1)[:,selector]
    index_bcsize = blockcell_size[:,blockcell_index]
    offset = (np.random.rand(3, N_samples) -.5) * index_bcsize
    sample_coords = coordlist[blockcell_index] + offset.T
    return sample_coords

def w1(data, extent, partbounds, trys=10):
    """
    Calculate the natural estimator of the 2-point correlation function.
    """
    n = len(data)
    r = n
    R_ = map(lambda foo: random_coords(r, extent), trys*[None])
    RR_ = map(lambda R: separation_hist(R, R, partbounds), R_)
    RR = np.array(RR_).mean(axis=0)
    DD = separation_hist(data, data, partbounds)
    with np.errstate(divide='ignore', invalid='ignore'):
        term1 = (r*(r-1.))/(n*(n-1.)) * DD/RR
        result = np.nan_to_num(term1) -1.
    return result
    
def w3(data, extent, partbounds, trys=10):
    """
    Calculate the Landy & Szalay estimator of the 2-point correlation function.
    """
    n = len(data)
    r = n
    R_ = map(lambda foo: random_coords(r, extent), trys*[None])
    RR_ = map(lambda R: separation_hist(R, R, partbounds), R_)
    RR = np.array(RR_).mean(axis=0)
    DR_ = map(lambda R: separation_hist(data, R, partbounds), R_)
    DR = np.array(DR_).mean(axis=0)
    DD = separation_hist(data, data, partbounds)
    with np.errstate(divide='ignore', invalid='ignore'):
        term1 = (r*(r-1.))/(n*(n-1.)) * DD/RR
        term2 = (r-1.)/n * DR/RR
        result = np.nan_to_num(term1-term2) +1.
    return result

def wth(data, extent, partbounds):
    """
    Calculate the theoretical 2-point correlation function.
    """
    domainsize = extent[:,1] -extent[:,0]
    n = len(data)
    DD = separation_hist(data, data, partbounds)
    DD_error = np.sqrt(DD)
    DXV = np.full_like(DD, np.inf, dtype=np.float64)
    DXV[1:-1] = 4./3.*np.pi*(partbounds[1:]**3-partbounds[:-1]**3)
    RR = DXV/np.prod(domainsize) * n*(n-1.)/2.
    return DD/RR-1., DD_error/RR
    
# ==== DATA PROCESSING =========================================================
def histline_sph(ffile, fgrid):
    tempselect = (ffile['temp'].ravel()<MaxTemp)
    sample_coords = draw_point_sample(ffile, fgrid, selector=tempselect, N_samples=N_res)
    print '-> Processing two point correlation function'
    sys.stdout.flush()
    pbounds, X = partition(dist_norm, dist_res)
    YT, YT_error = wth(sample_coords, fgrid.extent(), pbounds)
    return pbounds, YT
    
def ax_hist_sph(ax, ffile, fgrid):
    tempselect = (ffile['temp'].ravel()<MaxTemp)
    sample_coords = draw_point_sample(ffile, fgrid, selector=tempselect, N_samples=N_res)
    print '-> Processing two point correlation function'
    sys.stdout.flush()
    pbounds, X = partition(dist_norm, dist_res)
    data, data_error = wth(sample_coords, fgrid.extent(), pbounds)
    #
    print '-> Plotting result'
    sys.stdout.flush()
    YT = data[1:-1]
    YT_error = data_error[1:-1]
    ax.set(xlim=[0., pbounds[-1]/pc], ylim=[.9, 1e+4])
    ax.plot([0., pbounds[-1]/pc], [1.,1.], c='black', ls='dotted')
    #ax.plot(X/pc, YT+1., c='black', marker='x', ls='solid')
    ax1.errorbar(X/pc, YT+1., yerr=YT_error, fmt='x', c='black')
    ax.set_title('2-point correlation function of mass sample points')
    ax.set(xlabel='distance [pc]')
    ax.set(yscale='log', ylabel=r'relative incidence $\omega+1$')

# ==== TEST SETTINGS ===========================================================    
#plotfile = '/Users/mweis/CollFlow_runs/CF97L/3.TEST/CF97L_T3_hdf5_plt_cnt_0150'
plotfile = 'M:/scratchbin/MW_supermuc/CF97L/3.TEST/CF97L_T3_hdf5_plt_cnt_0190'

# ==== TEST ====================================================================    
if __name__ == '__main__':
    import scripts.libalpha.micflash as mflash
    ffile = mflash.plotfile(plotfile)
    ffile.learn(mflash.var_mhd)
    ffile.learn(mflash.var_ch5)
    ffile.learn(mflash.var_grid)
    fgrid = mflash.pm3dgrid(ffile)    

    fig, ax1 = plt.subplots(1, 1)
    ax_hist_sph(ax1, ffile, fgrid)
    fig.set_size_inches(14, 12, True)
    fig.tight_layout()
    plt.show()