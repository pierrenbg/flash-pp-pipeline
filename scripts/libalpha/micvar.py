# -*- coding: utf-8 -*-
import h5py
import numpy as np
from upnparser import upn
from collections import Iterable
import sys

__author__ = "Michael Weis"
__version__ = "0.1.4.0"

# ==== Constants ===============================================================
float_types = (np.float16, np.float32)
int_types = (np.int8, np.int16, np.int32)
uint_types = (np.uint8, np.uint16, np.uint32)

# ==== CLASS: flashfile data reader ============================================
class flashfile(object):    
    def __init__(self, filename):
        self.__filename = filename
        self.__plotfile = h5py.File(self.__filename, 'r')
    def __enter__(self):
        return self
    def __exit__(self, *exc):
        self.close()
        return False
    def __del__(self):
        self.close()
    def __repr__(self, level=0):
        answer  = 'HDF5 Flash Datafile' + '\n'
        answer += self.__filename+'\n'
        for key, dataset in self.__plotfile.iteritems():
            answer += '  '*level
            answer += str(dataset)+'\n'
        return answer
    def read_raw(self, path=''):
        clean_ds = lambda ds: { k.strip():v for k, v in dict(ds).iteritems()}
        #clean_ds = lambda ds: {(param['name'].rstrip(), param['value']) for param in ds}
        traverse = lambda ds, step: clean_ds(ds)[step]
        route = filter(lambda subdir: subdir!='', path.split('/'))
        return reduce(traverse, [self.__plotfile,]+route)
    def read(self, path, dtype='std'):
        data = self.read_raw(path)
        try:
            if isinstance(data.dtype.names, Iterable):
                raw_ds = dict((param['name'].rstrip(), param['value']) for param in data)
            else:
                raw_ds = data
        except:
            raw_ds = data
        try:
            if dtype == 'std':
                if raw_ds.dtype in float_types:
                    return np.array(raw_ds, dtype=np.float64)
                elif raw_ds.dtype in int_types:
                    return np.array(raw_ds, dtype=np.int64)
                elif raw_ds.dtype in uint_types:
                    return np.array(raw_ds, dtype=np.uint64)
                elif raw_ds.dtype.type is np.string_:
                    return [item.rstrip() for item in np.array(raw_ds).flatten()]
                else:
                    return np.array(raw_ds)
            else:
                return np.array(raw_ds, dtype=dtype)
        except:
            return raw_ds
    def list(self, path=''):
        dataset = self.read_raw(path)
        return [key.rstrip() for key in dataset]
    def filename(self):
        return self.__filename
    def close(self):
        try:
            self.__plotfile.close()
            print('Closing '+str(filename))
        except:
            pass
        
# ==== CLASS: memorizing plotfile evaluator ====================================
class plotfile(flashfile):
    def __init__(self, filename, leaveselect=True, memorize=True):
        flashfile.__init__(self, filename)
        nodetype = np.array(flashfile.read(self, 'node type'))
        self.__nodeselect = (nodetype==1) if leaveselect else (nodetype==nodetype)
        self.__memodict = dict()
        self.__formulae = dict()
        self.__memorize = memorize
    def __enter__(self):
        flashfile.__enter__(self)
        return self
    def __exit__(self, *exc):
        self.close()
        return False
    def __del__(self):
        self.close()
    def __repr__(self, level=0):
        return flashfile.__repr__(self, level)
    def read(self, key):
        """ Remove inner nodes (if applicable) prior to returning flashfile data """
        data = flashfile.read(self, key)
        try:
            if len(data) == len(self.__nodeselect): # len(data) might be impossible!
                return data[self.__nodeselect]
            else:
                return data
        except:
            return data
    def cache(self, key, dataset, force=True):
        if self.__memorize or force:
            self.__memodict[key] = dataset
        return dataset
    def cache_fields(self, fields):
        for key in fields:
            self.cache(key, fields[key], force=True)
        return fields.keys()
    def __getitem__(self, key_in):
        #print('get %s'%key_in)
        #sys.stdout.flush()
        # Remove plot setting specifier (indicated by '::') from key:
        key = key_in.split('::')[0]
        # 1. Look up, if key is memorized in dictionary
        if key in self.__memodict:
            return self.__memodict[key]
        # 2. If not memorized, read/calculate data
        else:
            # 2a. Try to obtain data from file
            try:
                dataset = plotfile.read(self, key)
            # 2b. If variable not in file, try to obtain data by formula
            except:
                if key in self.__formulae.keys():
                    ret = self.__formulae[key].eval()
                    return self.cache(key, ret, force=False)
                else:
                    raise KeyError('Plotfile: Unknown variable "'+str(key)+'"')
            else:
                # 3a. Data successfully obtained from file.
                #     Try to filter leaves, then memorize data
                return self.cache(key, dataset, force=False)
    def learnvar(self, key, upn_form):
        self.__formulae[key] = upn(upn_form, str_eval=self.__getitem__)
    def learn(self, vardict):
        for key in vardict:
            self.learnvar(key, vardict[key])
    def close(self):
        flashfile.close(self)
        self.__memodict.clear()        
        self.__formulae.clear()        

# ==== RPN Formulae for some ubiquitous mhd./chem. vars. =======================
k_b = 1.38064852e-16    # Boltzmann constant (erg/K)
m_a = 1.660539040e-24     # atomic mass unit (g)/(Da)
G_g = 6.67408e-8    # Gravitational constant (cgs)
sq = np.square
lg = np.log10
#sclip = lambda x: np.clip(x, -1, 1)
var_grid = {
    'nxb': ('integer scalars/nxb',),
    'nyb': ('integer scalars/nyb',),
    'nzb': ('integer scalars/nzb',),
    'vol': ('block size', lambda a: np.reshape(np.prod(a, axis=-1), (-1,1,1,1)),
        'dens', np.ones_like, '*', 'nxb', '/', 'nyb', '/', 'nzb', '/'),
    'lenx': ('block size', lambda a: np.reshape(a[...,0], (-1,1,1,1)),
        'dens', np.ones_like, '*', 'nxb', '/'),
    'leny': ('block size', lambda a: np.reshape(a[...,1], (-1,1,1,1)),
        'dens', np.ones_like, '*', 'nyb', '/'),
    'lenz': ('block size', lambda a: np.reshape(a[...,2], (-1,1,1,1)),
        'dens', np.ones_like, '*', 'nzb', '/'),
    'length': ('vol', 1./3., '**'),
    'mass': ('dens', 'vol', '*'),
    'parts': ('numdens', 'vol', '*'),
    'rlevel': ('refine level', lambda x: np.reshape(x, (-1,1,1,1)),
        'dens', np.ones_like, '*'),
    'denscontrast_m': ('mass', np.sum, 'dens', 'mass', '*', np.sum, '/', 'dens', '*'),
    'denscontrast_v': ('vol', np.sum, 'dens', 'vol', '*', np.sum, '/', 'dens', '*'),
}

var_mhd = {
    'jx': ('dens', 'velx', '*'),
    'jy': ('dens', 'vely', '*'),
    'jz': ('dens', 'velz', '*'),
    'numdens': ('pres', 'temp', k_b, '*', '/'), # Particle number density
    'mu': ('dens', 'numdens', '/'), # Mean particle mass
    'Ai': ('mu', m_a, '/'), # Mean nucleon number
    'mag_sq': ('magx', sq, 'magy', sq, 'magz', sq, '+', '+'),
    'mag': ('mag_sq', np.sqrt),
    'magp': ('mag_sq', (1./(8.*np.pi)), '*'),
    'mag_yz': ('magy', sq, 'magz', sq, '+', np.sqrt),
    'beta': ('pres', 'magp', '/'),
    'vel_sq': ('velx', sq, 'vely', sq, 'velz', sq, '+', '+'),
    'vel': ('vel_sq', np.sqrt),
    'velp': ('vel_sq', 'dens', '*', .5, '*'),
    'vel_yz': ('vely', sq, 'velz', sq, '+', np.sqrt),
    'crossp': ('vely', sq, 'velz', sq, '+', 'dens', '*', .5, '*'),
    'beta_v': ('velp', 'magp', '/'),
    'epot': ('gpot', 'gpot', np.max, '-'),
    'gravp': ('gpot', 'dens', '*'),
    'p_sum': ('magp', 'velp', '+', 'gravp', '+', 'pres', '+'),
    'c_s': ('pres', 'dens', '/', np.sqrt),
    'c_a': ('mag', 4*np.pi, 'dens', '*', np.sqrt, '/'),
    'mach_s': ('vel_sq', np.sqrt, 'c_s', '/'),
    'mach_a': ('vel_sq', np.sqrt, 'c_a', '/'),
    't_ff': (1., G_g, 'dens', '*', np.sqrt, '/'),
    'l_jeans': ('c_s', 't_ff', '*', np.sqrt(np.pi), '*'), # Jeans length as oscillation wavelength
    'jeans_res': ('l_jeans', 'length', '/'),
    'sel_slab': ('p_sum', 0., '<'),
    'sel_inflow': ('p_sum', 0., '>'),
    'vb_cos': ('magx', 'velx', '*', 'magy', 'vely', '*', '+',
        'magz', 'velz', '*', '+', 'mag_sq', 'vel_sq', '*', np.sqrt, '/'),
    'vb_oangle': ('vb_cos', lambda x:np.clip(x,-1,1), np.arccos, 180./np.pi, '*'),
    'vb_angle': ('vb_cos', lambda x:np.clip(np.fabs(x),0,1), np.arccos, 180./np.pi, '*'),
}

ch_mu5 = {
    'Ha': 1.0, 'He': 4.002602, 'C': 12.011, 'O': 15.9994, 'Si': 28.0855,
    'H2': 2.0, 'Hp': 1.0, 'Cp': 12.011, 'CO': 28.01,}
var_ch5 = {
    'ch_abundhe': ('real runtime parameters/ch_abundhe',),
    'ch_abundc': ('real runtime parameters/ch_abundc',),
    'ch_abundo': ('real runtime parameters/ch_abundo',),
    'ch_abundsi': ('real runtime parameters/ch_abundsi',),
    'ch_abar': (ch_mu5['Ha'],
        ch_mu5['He'], 'ch_abundhe', '*', '+',
        ch_mu5['C'], 'ch_abundc', '*', '+',
        ch_mu5['O'], 'ch_abundo', '*', '+',
        ch_mu5['Si'], 'ch_abundsi', '*', '+',),
    'ch_ihp': ('ihp', 'ch_abar', '/'),
    'ch_iha': ('iha', 'ch_abar', '/'),
    'ch_ih2': ('ih2', 'ch_abar', '/'),
    'ch_icp': ('icp', 'ch_abar', '/'),
    'ch_ico': (ch_mu5['CO']/ch_mu5['Cp'], 'ico', '*', 'ch_abar', '/'), 
    'n_hp': ('dens', 'ch_ihp', '*', m_a*ch_mu5['Hp'], '/'),
    'n_ha': ('dens', 'ch_iha', '*', m_a*ch_mu5['Ha'], '/'),
    'n_h2': ('dens', 'ch_ih2', '*', m_a*ch_mu5['H2'], '/'),
    'n_cp': ('dens', 'ch_icp', '*', m_a*ch_mu5['Cp'], '/'),
    'n_co': ('dens', 'ch_ico', '*', m_a*ch_mu5['CO'], '/'),
    'n_h1': ('n_ha', 'n_hp', '+'),
    'n_e': ('n_hp', 'n_cp', '+'),
    'n_hx': ('n_ha', 'n_hp', '+', 'n_h2', 2, '*', '+'),
    'n_cx': ('n_co', 'n_cp', '+'),
    'relrate_h2': ('dih2', 'iha', 'ihp', '+', '/'),
    'relrate_co': ('dico', 'icp', '/'),
    'densrate_h2': ('relrate_h2', 'dens_ha', 'dens_hp', '+', '*'),
    'densrate_co': ('relrate_co', 'dens_cp', '*', ch_mu5['CO']/ch_mu5['Cp'], '*'),
    'nrate_h2': ('densrate_h2', m_a*ch_mu5['H2'], '/'),
    'nrate_co': ('densrate_co', m_a*ch_mu5['CO'], '/'),
    'madens': ('dens', m_a, '/', 'ch_abar', '/'), # density in units of H-atom-m/ccm
}

ch_mu15 = {
    'Ha': 1.008, 'He': 4.002602, 'C': 12.011, 'O': 15.9994, 'Si': 28.0855,
    'H2': 2.01588, 'Hp': 1.0072, 'Hep': 4.002602, 'CHx': 13., 'OHx': 17., 
    'Cp': 12.011, 'HCOp': 29., 'CO': 28.01, 'Mp': 28., 'M':28.}

var_ch15 = { ### TODO: WORK IN PROGRESS!
    'ch_ihp': ('ihp',),
    'ch_iha': ('iha',),
    'ch_ih2': ('ih2',),
    'ch_icp': ('icp',),
    'ch_ico': ('ico',), 
    'n_hp': ('dens', 'ch_ihp', '*', m_a*ch_mu15['Hp'], '/'),
    'n_ha': ('dens', 'ch_iha', '*', m_a*ch_mu15['Ha'], '/'),
    'n_h2': ('dens', 'ch_ih2', '*', m_a*ch_mu15['H2'], '/'),
    'n_cp': ('dens', 'ch_icp', '*', m_a*ch_mu15['Cp'], '/'),
    'n_co': ('dens', 'ch_ico', '*', m_a*ch_mu15['CO'], '/'),
    'n_h1': ('n_ha', 'n_hp', '+'),
    'n_e': ('n_hp', 'n_cp', '+'),
    'n_hx': ('n_ha', 'n_hp', '+', 'n_h2', 2, '*', '+'),
    'n_cx': ('n_co', 'n_cp', '+'),
    'relrate_h2': ('dih2', 'iha', 'ihp', '+', '/'),
    'relrate_co': ('dico', 'icp', '/'),
    'densrate_h2': ('relrate_h2', 'dens_h1', '*'),
    'densrate_co': ('relrate_co', 'dens_cp', '*'),
    'nrate_h2': ('densrate_h2', m_a*ch_mu15['H2'], '/'),
    'nrate_co': ('densrate_co', m_a*ch_mu15['CO'], '/'),
    'madens': ('dens', m_a*ch_mu5['Ha'], '/'), # density in units of H-atom-m/ccm
}

var_chx = {
    'dens_hp': ('dens', 'ch_ihp', '*'),
    'dens_ha': ('dens', 'ch_iha', '*'),
    'dens_h2': ('dens', 'ch_ih2', '*'),
    'dens_cp': ('dens', 'ch_icp', '*'),
    'dens_co': ('dens', 'ch_ico', '*'),
    'dens_h1': ('dens_ha', 'dens_hp', '+'),
    'mass_hp': ('dens_hp', 'vol', '*'),
    'mass_ha': ('dens_ha', 'vol', '*'),
    'mass_h2': ('dens_h2', 'vol', '*'),
    'mass_cp': ('dens_cp', 'vol', '*'),
    'mass_co': ('dens_co', 'vol', '*'),
    'mass_h1': ('dens_h1', 'vol', '*'),
    'massfrac_hp': ('mass_hp', 'mass', '/'),
    'massfrac_ha': ('mass_ha', 'mass', '/'),
    'massfrac_h2': ('mass_h2', 'mass', '/'),
    'massfrac_cp': ('mass_cp', 'mass', '/'),
    'massfrac_co': ('mass_co', 'mass', '/'),
    'massfrac_h1': ('mass_h1', 'mass', '/'),
    'abund_hp': ('n_hp', 'madens', '/'),
    'abund_ha': ('n_ha', 'madens', '/'),
    'abund_h2': ('n_h2', 'madens', '/'),
    'abund_cp': ('n_cp', 'madens', '/'),
    'abund_co': ('n_co', 'madens', '/'),
    'abund_h1': ('n_h1', 'madens', '/'),
    'abund_e': ('n_e', 'madens', '/'),
    'massrate_h2': ('dih2', 'ih2', '/', 'mass_h2', '*'),
    'massrate_co': ('dico', 'ico', '/', 'mass_co', '*'),
    'abco_sel': ('abund_co', 1e-4, '>'),
    'abco_dens': ('abco_sel', 'dens', '*'),
    'ionrate': ('n_e', 'numdens', '/', 2., '*'),
}

var_ch5.update(var_chx)
var_ch15.update(var_chx)

# ==== TEST ====================================================================
if __name__ == '__main__':
    filename = 'M:/scratchbin/MW_CF97_supermuc/CF97J/1.TEST/CF97T1_hdf5_plt_cnt_0048'
    ffile = plotfile(filename)
    ffile.learn(var_grid)
    ffile.learn(var_mhd)
    ffile.learn(var_ch5)
