# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import numpy as np
import matplotlib.pylab as plt
import sys, fnmatch, os
from ast import literal_eval
import matplotlib as mpl
import matplotlib.colors as cl
from mpl_toolkits.axes_grid1 import make_axes_locatable

__author__ = "Michael Weis"
__version__ = '0.1.2.0'

__verbose__ = False

# ==== HELPER ==================================================================
def flushprint(*args):
    for arg in args:
        print(arg),
    print
    sys.stdout.flush()

def verboseprint(verbose, *args):
    if verbose:
        for arg in args:
            print(arg),
        print
        sys.stdout.flush()

def timestr():
    import time
    t = time.gmtime()
    tstr  = "%04d%02d%02d"%(t.tm_year,t.tm_mon,t.tm_mday)+'-'
    tstr += "%02d%02d%02d"%(t.tm_hour,t.tm_min,t.tm_sec)+'-'
    tstr += 'R'+str(np.random.randint(100))
    return tstr

# ==== TICK POSITIONING HELPER =================================================
def filter_ticks(tryticks, norm, nmin):
    """
    get_ticks subfunction:
    Filter ticks to display only those having a sufficient distance.
    """
    vmin, vmax = norm.inverse(0), norm.inverse(1)
    a = np.array(tryticks)
    rangeticks = np.sort(a[(a>=vmin)*(a<=vmax)])
    relpos = norm(rangeticks)
    delta = relpos[1:]-relpos[:-1]
    deltamin = 1./(2.*(nmin-1))
    if len(rangeticks) == 0:
        return []
    ticks = [rangeticks[0], ]
    dsum = 0.
    for i, d in enumerate(delta):
        dsum += d
        if dsum>deltamin:
            ticks.append(rangeticks[i+1])
            dsum = 0.
    return list(set(ticks))

def get_ticks(norm, nmin=6):
    from matplotlib.colors import Normalize
    v0, v1 = norm.inverse(0), norm.inverse(1)
    vmin, vmax = min(v0,v1), max(v0,v1)
    if vmin>0:
        floororder = np.floor(np.log10(vmin))
        orderticks = 10**np.arange(floororder, np.ceil(np.log10(vmax)))
    else:
        orderticks = np.array([])
    ticks = filter_ticks(orderticks, norm, nmin)
    if len(ticks)<nmin:
        tryticks = [vmin,] + list(orderticks) + [vmax,]
        ticks = filter_ticks(tryticks, norm, nmin)
    if len(ticks)<nmin:
        tryticks += list(2.*orderticks)
        tryticks += list(5.*orderticks)
        ticks = filter_ticks(tryticks, norm, nmin)
    if len(ticks)<nmin:
        order = np.int(np.rint(np.log10(vmax-vmin)))
        tryticks += list(np.round(norm.inverse(np.linspace(0, 1, nmin+1)), -order+3))
        ticks = filter_ticks(tryticks, norm, nmin)
    tickpos = Normalize(vmin, vmax).inverse(norm(ticks))
    ticklabel = ['%.4g'%tick for tick in ticks]
    return ticklabel, tickpos

# ==== FILE ACQUISITION HELPER =================================================
def multifilearg(*arg):
    from os.path import split, join, isdir, isfile
    files = list()
    for pattern in arg:
        dirname, filepattern = (arg, '*') if isdir(arg) else split(arg)
        dirfiles = [f for f in os.listdir(dirname) if isfile(f)]
        matchfiles = fnmatch.filter(dirfiles, filepattern)
        filepathes = [join(dirname, fn) for fn in matchfiles]
        files.extend(filepathes)
    return files

def process(function, filename, verbose=False, strict=False):
    verboseprint(verbose, '\n-> Opening :', filename)
    if not strict:
        try:
            result = function(filename)
        except Exception as e:
            result = None
            flushprint('-> Error   :', e)
            flushprint('-> Skipped :', filename)
        else:
            verboseprint(verbose, '-> Success :', filename)
    else:
        result = function(filename)
        verboseprint(verbose, '-> Success :', filename)        
    return result

def batchprocess(function, filebatch, verbose=False, strict=False):
    if hasattr(filebatch, '__iter__'):
        resultlist = []
        for entry in filebatch:
            resultlist.append(batchprocess(function, entry, verbose, strict=strict))
        return resultlist
    else:
        filename = filebatch
        return process(function, filename, verbose=verbose, strict=strict)

def countbatch(path, basename, cntrange, cntdigits=4):
    """ Scan the given directory for plotfiles in subdirectories count wise"""
    batch = []
    counts = []
    for cnt in cntrange:
        plotfiles = []
        pattern = basename+str(cnt).zfill(cntdigits)
        for dirpath, dirnames, files in os.walk(path, followlinks=True):
            # print cnt, dirpath
            for filename in fnmatch.filter(files, pattern):
                if '_forced_' in filename:
                    continue
                plotfiles.append(os.path.join(dirpath, filename))
        if plotfiles:
            batch.append(plotfiles)
            counts.append(cnt)
    return counts, batch
  
def dirbatch(path, basename, cntrange, cntdigits=4):
    """ Scan the given directory for plotfiles subdirectory wise"""
    dirdict = {}
    patternlist = [basename+str(cnt).zfill(cntdigits) for cnt in cntrange]
    for dirpath, dirnames, files in os.walk(path, followlinks=True):
        for pattern in patternlist:
            for filename in fnmatch.filter(files, pattern):
                if '_forced_' in filename:
                    continue
                plotfile = os.path.join(dirpath, filename)
                if not dirpath in dirdict:   
                    dirdict[dirpath] = [plotfile,]
                else:
                    dirdict[dirpath].append(plotfile)        
    return dirdict

def matrixbatch(path, basename, cntrange, cntdigits=4):
    """ ??? """
    dirdict = {}
    patternlist = [basename+str(cnt).zfill(cntdigits) for cnt in cntrange]
    for dirpath, dirnames, files in os.walk(path, followlinks=True):
        for ipa, pattern in enumerate(patternlist):
            for filename in fnmatch.filter(files, pattern):
                if '_forced_' in filename:
                    continue
                else:
                    if not dirpath in dirdict:   
                        dirdict[dirpath] = len(patternlist)*[None,]
                    dirdict[dirpath][ipa] = os.path.join(dirpath, filename)
                    break
    return dirdict
    
# ==== PARAMETER FILE PARSER ===================================================
def safe_eval(value):
    value_dict = {'.true.': True, '.false.': False}
    if value in value_dict:
        return value_dict[value]
    try:
        # known to crash on '.true.', '.false.', may go wrong in other cases.
        return literal_eval(value)
    except:
        # no clue how to evaluate (e.g. string like 'periodic'). Leave it as is.
        return value
        
def readpar(filename):
    par_dict = {'plot_vars': set()}
    with open(filename, 'r') as infile:
        for line in infile:
            # cut line prior to first # :
            line = line.rsplit('#')[0]
            # omit line if not containing = :
            if '=' not in line:
                continue
            # Transfer key and value from line to dictionary:
            key, foo, value = [word.strip() for word in line.rpartition('=')]
            if key.startswith('plot_var_'):
                par_dict['plot_vars'].add(value)
            else:
                par_dict[key] = safe_eval(value)
    return par_dict
    
# ==== Linear Interpolation Helper =============================================
def lin_interpolator(X_base, Y_base):
    I = np.argsort(X_base)
    X = np.array(X_base)[I]
    Y = np.array(Y_base)[I]
    Xmin, Xmax = X[[0,-1]]
    dX = X[1:]-X[:-1]
    def interpolate(X_in):
        X_in = np.array(X_in)
        Ifloor = np.clip(np.searchsorted(X, X_in)-1, 0, len(dX)-1)
        Ifrac = (X_in-X[Ifloor])/dX[Ifloor]
        return ((1.-Ifrac).T*(Y[Ifloor]).T + (Ifrac).T*(Y[Ifloor+1]).T).T
    return interpolate
    

# ==== 2D-Histogram-Helper =====================================================
def bincount2d(Ix, Iy, weights, minlenx=None, minleny=None):
    lenx = max(np.max(Ix)+1, minlenx)
    leny = max(np.max(Iy)+1, minleny)
    Iflat = Ix*leny + Iy
    n = len(Ix)/100000+1
    countsflat = np.bincount(Iflat[0::n], weights=weights[0::n], minlength=lenx*leny)
    for i in xrange(1,n):
        countsflat += np.bincount(Iflat[i::n], weights=weights[i::n], minlength=lenx*leny)
    return countsflat.reshape(lenx, leny)

class digiscatter(object):
    def __init__(self, X, Y, x_norm=None, y_norm=None, x_res=400, y_res=400,
                 x_unit=1., y_unit=1.):
        # Select a x-norm if not given 
        x_data = X/x_unit
        if x_norm is None:
            x_min, x_max = np.percentile(x_data, 3), np.percentile(x_data, 97)
            self.x_norm = cl.LogNorm(x_min, x_max) if (x_min*x_max>0.) else cl.Normalize(x_min, x_max)
        else:
            self.x_norm = x_norm
        # Select a y-norm if not given 
        y_data = Y/y_unit
        if y_norm is None:
            y_min, y_max = np.percentile(y_data, 3), np.percentile(y_data, 97)
            self.y_norm = cl.LogNorm(y_min, y_max) if (y_min*y_max>0.) else cl.Normalize(x_min, x_max)
        else:
            self.y_norm = y_norm
        #
        self.x_res=x_res
        self.y_res=y_res
        #
        self.x_partition = x_norm.inverse(np.linspace(0, 1, x_res+1))
        self.y_partition = y_norm.inverse(np.linspace(0, 1, y_res+1))
        #
        self.x_bins = np.searchsorted(self.x_partition, x_data.flatten())
        self.y_bins = np.searchsorted(self.y_partition, y_data.flatten())
        #
        self.extent = [self.x_norm.inverse(0), self.x_norm.inverse(1),
                       self.y_norm.inverse(0), self.y_norm.inverse(1)]
        
    def heatmap(self, weights=None):
        lenx = self.x_res+2
        leny = self.y_res+2
        xy_histo = bincount2d(self.x_bins, self.y_bins, weights, lenx, leny)
        return xy_histo[1:-1,1:-1]
        
    def ax_scatter(self, ax, weight_data=None, cbar=True, **kwargs):
        if weight_data is None:
            weight_data = np.ones_like(self.x_bins, dtype=np.float64)
        weights = weight_data/weight_data.sum()*self.x_res*self.y_res
        xy_data = self.heatmap(weights)
        #
        vmax = self.x_res*self.y_res*.5
        vmin = vmax*1e-7
        np.clip(xy_data, vmin, vmax, xy_data)        
        im = ax.imshow(xy_data.T, origin="lower", interpolation='none',
            norm=cl.LogNorm(vmin=vmin, vmax=vmax),
            extent=self.extent, aspect='auto', **kwargs)
        ax.set(xlim=self.extent[0:2], ylim=self.extent[2:4])
        #
        xticks, xtickpos = get_ticks(self.x_norm)
        ax.set_xticks(xtickpos)
        ax.set_xticklabels(xticks)
        yticks, ytickpos = get_ticks(self.y_norm)
        ax.set_yticks(ytickpos)
        ax.set_yticklabels(yticks)
        #
        if cbar:
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size="4%", pad=0.1)
            cbar = plt.colorbar(im, cax=cax)
            return im, cbar
        else:
            return im
            
    def ax_colscatter(self, ax, C, c_norm=None, c_unit=1., Cw_data=None, Yw_data=None, **kwargs):
        # Select a x-norm if not given 
        c_data = C/c_unit
        if c_norm is None:
            c_min, c_max = np.percentile(c_data, 3), np.percentile(c_data, 97)
            self.x_norm = cl.LogNorm(c_min, c_max) if (c_min*c_max>0.) else cl.Normalize(c_min, c_max)
        else:
            self.c_norm = c_norm
        #
        vmax = self.x_res*self.y_res*.5
        vmin = vmax*1e-7
        #
        if Cw_data is None:
            cw_data = np.ones_like(self.x_bins, dtype=np.float64)
        c_weights = cw_data/cw_data.sum()*self.x_res*self.y_res
        c_weight_histo = self.heatmap(c_weights)
        loads =  c_data.flatten() * c_weights
        load_histo = self.heatmap(loads)
        xyC_data = load_histo/(c_weight_histo+1e-20*vmax)
        #
        if Yw_data is None:
            Yw_data = np.ones_like(self.x_bins, dtype=np.float64)
        weights = Yw_data/Yw_data.sum()*self.x_res*self.y_res
        xyY_data = self.heatmap(weights)
        #
        Y_norm = cl.LogNorm(vmin, vmax, clip=True)
        Y = Y_norm(np.clip(xyY_data, vmin, vmax))
        C = c_norm(np.clip(xyC_data, c_norm.inverse(0), c_norm.inverse(1)))
        RGB_map = RGB(1.-Y, -1.8+1.2*np.pi*C)
        #
        im = ax.imshow(RGB_map, origin="lower", interpolation='none',
            extent=self.extent, aspect='auto')
        ax.set(xlim=self.extent[0:2], ylim=self.extent[2:4])
        #
        divider = make_axes_locatable(ax)
        c1ax = divider.append_axes("right", size="4%", pad=0.1)
        cb1 = mpl.colorbar.ColorbarBase(c1ax, cmap='Y_map', norm=Y_norm,
            orientation='vertical')
        #
        c2ax = divider.append_axes("bottom", size="4%", pad=0.6)
        cb2 = mpl.colorbar.ColorbarBase(c2ax, cmap='C_map', norm=c_norm,
            orientation='horizontal')
        #
        xticks, xtickpos = get_ticks(self.x_norm)
        ax.set_xticks(xtickpos)
        ax.set_xticklabels(xticks)
        yticks, ytickpos = get_ticks(self.y_norm)
        ax.set_yticks(ytickpos)
        ax.set_yticklabels(yticks)
        #
        return im, (cb1, cb2)
    
# ==== color value manipulation helper =========================================
def rescale(X, Ymin=0.05, Ymax=0.95, Xmin=0., Xmax=1.):
    np.clip(X, Xmin, Xmax, X)
    Xn = (X -Xmin) / (Xmax -Xmin)
    Y = Ymin +(Ymax-Ymin)*Xn
    return Y


# ==== color model base vectors ===============================================
# Luminance model: Percieved luminance Y:
# Y = KR*R +KG*G +KB*B and KR +KG +KB = 1
# K = KR, KG, KB
K_color = dict()
K_color['NTSC'] = np.array((.3, .59, .11))
K_color['PAL'] = np.array((.299, .587, .114))
K_color['HDTV'] = np.array((.2126, .7152, .0722))
K_color['RGB'] = np.array((1./3., 1./3., 1./3.))
K_std_model = 'HDTV'


# ==== YPbPr color space converter =============================================
def RGB_to_YPbPr(R, G, B, model=K_std_model):
    # Read coefficients from color model
    KR, foo, KB = K_color[model]
    KG = 1. -KR -KB
    # Convert
    Y = KR*R +KG*G +KB*B
    Pr = .5*(R-Y)/(1.-KR)
    Pb = .5*(B-Y)/(1.-KB)
    return Y, Pb, Pr

def YPbPr_to_RGB(Y, Pb, Pr, model=K_std_model):
    # Read coefficients from color model
    KR, foo, KB = K_color[model]
    KG = 1. -KR -KB
    # Convert
    R = Y +2.*Pr*(1.-KR)
    B = Y +2.*Pb*(1.-KB)
    G = (Y -KR*R -KB*B) / KG
    return R, G, B


# ==== HSY color space converter ===============================================
def HCY_to_RGB(H, C, Y, model=K_std_model):
    # Read coefficients from color model
    KR, foo, KB = K_color[model]
    KG = 1. -KR -KB
    # Reconstruct Hue information
    a = C * np.cos(H)
    b = C * np.sin(H)
    #
    x = np.sqrt(1./3.)
    R = Y +(KG+KB)*a +x*(KB-KG)*b
    G = Y -KR*a      +x*(KR+2.*KB)*b
    B = Y -KR*a      -x*(KR+2.*KG)*b
    return R, G, B

def HY_to_Cmax(H, Y, model):
    RGBx = np.array(HCY_to_RGB(H, 1., Y, model=model))
    dRGBx = RGBx -Y
    dx_neg = np.min(dRGBx, axis=0)
    dx_pos = np.max(dRGBx, axis=0)
    with np.errstate(divide='ignore', invalid='ignore'):
        Cmax_neg =   (-Y) / dx_neg
        Cmax_pos = (1.-Y) / dx_pos 
        Cmax = np.fmin(Cmax_neg, Cmax_pos)
    return Cmax

def HSY_to_RGB(H, S, Y, model=K_std_model):
    C = S * HY_to_Cmax(H, Y, model)
    R, G, B = HCY_to_RGB(H, C, Y, model=model)
    return R, G, B

def RGB_to_HSY(R, G, B, model=K_std_model):
    # Read coefficients from color model
    KR, foo, KB = K_color[model]
    KG = 1. -KR -KB
    # Calculate Luma
    Y = KR*R +KG*G +KB*B
    # Calculate Hue
    a = R -.5*G -.5*B
    b = np.sqrt(.75)*(G-B)
    H = np.arctan2(b, a)
    # Calculate Saturation
    C = np.sqrt(a**2 +b**2)
    with np.errstate(divide='ignore', invalid='ignore'):
        S = np.nan_to_num(C / HY_to_Cmax(H, Y, model))
    return H, S, Y


# ==== COLORMAP FADER ==========================================================
def transform_cmap(cmap, H=None, S=None, Y=None, name=None):
    X = np.linspace(0., 1., 1025)
    R, G, B, A = cmap(X).T
    H_X, S_X, Y_X = RGB_to_HSY(R, G, B)

    if H is None:
        H = H_X
    if S is None:
        S = S_X
    if Y is None:
        Y = Y_X
    

    Rx, Gx, Bx = HSY_to_RGB(H, S, Y)
    cdict = {'red':  [(x,Rx[i],Rx[i]) for i,x in enumerate(X)],
            'green': [(x,Gx[i],Gx[i]) for i,x in enumerate(X)],
            'blue':  [(x,Bx[i],Bx[i]) for i,x in enumerate(X)],}

    cmap_name = str(hash(str(cdict))) if name is None else name
    plt.register_cmap(name=cmap_name, data=cdict)
    return plt.get_cmap(cmap_name)


def build_faded_cmap(cmap_str, sat=None, Ymin=.01, Ymax=.85):
    cmap = plt.get_cmap(cmap_str)

    X = np.linspace(0., 1., 1025)
    R, G, B, A = cmap(X).T
    H, S, Y = RGB_to_HSY(R, G, B)

    S_new = S if sat is None else sat
    Y_new = rescale(X, Ymin=Ymin, Ymax=Ymax)
    Rx, Gx, Bx = HSY_to_RGB(H, S_new, Y_new)

    cdict = {'red':  [(x,Rx[i],Rx[i]) for i,x in enumerate(X)],
            'green': [(x,Gx[i],Gx[i]) for i,x in enumerate(X)],
            'blue':  [(x,Bx[i],Bx[i]) for i,x in enumerate(X)],}
    plt.register_cmap(name='f_'+cmap_str, data=cdict)

    Y_new_r = rescale(1.-X, Ymin=Ymin, Ymax=Ymax)
    Rxr, Gxr, Bxr = HSY_to_RGB(H, S_new, Y_new_r)
    cdict = {'red':  [(x,Rxr[i],Rxr[i]) for i,x in enumerate(X)],
            'green': [(x,Gxr[i],Gxr[i]) for i,x in enumerate(X)],
            'blue':  [(x,Bxr[i],Bxr[i]) for i,x in enumerate(X)],}
    plt.register_cmap(name='fr_'+cmap_str, data=cdict)


# ==== HELIX-RGB-HELPER ========================================================
cR, cG, cB = K_color[K_std_model]
# Define corresponding orthonormal (R,G,B) vectors describing a plane of
# constant luminance, i.e. Y(R,G,B) = 0:
v1  = np.array((-cR, -cG, (cR**2+cG**2)*1./cB))
v1 /= np.linalg.norm(v1)
v2  = np.array((cG, -cR, 0.))
v2 /= np.linalg.norm(v2)
Rv = np.vstack((v1,v2))
def RGB(Y, phi, hue=1., gamma=1.):
    n = np.array((np.cos(phi), np.sin(phi)))
    amp = hue*(Y**gamma)*(1.-Y**gamma)
    RGB = Y**gamma +amp*np.dot(n.T, Rv).T
    return RGB.T

X = np.linspace(0., 1., 1025)

Y_RGB = lambda Y: RGB(1.-Y, .5+0.*Y, hue=0.)
R, G, B = Y_RGB(X).T
cdict = {'red':  [(x,R[i],R[i]) for i,x in enumerate(X)],
        'green': [(x,G[i],G[i]) for i,x in enumerate(X)],
        'blue':  [(x,B[i],B[i]) for i,x in enumerate(X)],}
plt.register_cmap(name='Y_map', data=cdict)

C_RGB = lambda C: RGB(.5+0.*C, -1.8+1.2*np.pi*C)
R, G, B = C_RGB(X).T
cdict = {'red':  [(x,R[i],R[i]) for i,x in enumerate(X)],
        'green': [(x,G[i],G[i]) for i,x in enumerate(X)],
        'blue':  [(x,B[i],B[i]) for i,x in enumerate(X)],}
plt.register_cmap(name='C_map', data=cdict)


# ==== Function: helix colormap constructor ====================================
def build_helixmap(name, start=.5, rotations=-1.5, sat=1.0, gamma=1.0,
                   Ymin=0., Ymax=1., model=K_std_model):

    cR, cG, cB = K_color[model]
    
    # Define corresponding orthonormal (R,G,B) vectors describing a plane of
    # constant luminance, i.e. Y(R,G,B) = 0:
    v1  = np.array((-cR, -cG, (cR**2+cG**2)*1./cB))
    v1 /= np.linalg.norm(v1)
    v2  = np.array((cG, -cR, 0.))
    v2 /= np.linalg.norm(v2)
    Rv = np.vstack((v1,v2))
    
    def helixmap(x):
        # Calculate Luma
        Y = rescale(x**gamma, Ymin=Ymin, Ymax=Ymax)
        # Calculate Hue vector
        H = 2.*np.pi*(start*1./3.+rotations*x)
        n = np.array((np.cos(H), np.sin(H)))
        Hn = np.dot(n.T, Rv).T
        # Calculate RGB
        C = sat*Y*(1.-Y)
        RGB = Y +C*Hn
        return RGB
        
    X = np.linspace(0., 1., 1025)
    
    R,G,B = helixmap(X)
    cdict = {'red':  [(x,R[i],R[i]) for i,x in enumerate(X)],
            'green': [(x,G[i],G[i]) for i,x in enumerate(X)],
            'blue':  [(x,B[i],B[i]) for i,x in enumerate(X)],}
    plt.register_cmap(name=name, data=cdict)

    R,G,B = helixmap(1.-X)
    cdict = {'red':  [(x,R[i],R[i]) for i,x in enumerate(X)],
            'green': [(x,G[i],G[i]) for i,x in enumerate(X)],
            'blue':  [(x,B[i],B[i]) for i,x in enumerate(X)],}
    plt.register_cmap(name=name+'_r', data=cdict)
        

# ==== Function: saturated helix colormap constructor ==========================
def build_shelixmap(name, start=2.5, rotations=-1.5, sat=.75, gamma=1.0,
                    Ymin=0.05, Ymax=0.95, model=K_std_model):
        
    def helixmap(x):
        # Calculate Luma
        Y = rescale(x**gamma, Ymin=Ymin, Ymax=Ymax)
        # Calculate Hue
        H = 2.*np.pi*(start*1./3.+rotations*x)
        #
        RGB = np.array(HSY_to_RGB(H, sat, Y, model=model))
        return RGB

    X = np.linspace(0., 1., 65537)
        
    R,G,B = helixmap(X)
    cdict = {'red':  [(x,R[i],R[i]) for i,x in enumerate(X)],
            'green': [(x,G[i],G[i]) for i,x in enumerate(X)],
            'blue':  [(x,B[i],B[i]) for i,x in enumerate(X)],}
    plt.register_cmap(name=name, data=cdict)

    R,G,B = helixmap(1.-X)
    cdict = {'red':  [(x,R[i],R[i]) for i,x in enumerate(X)],
            'green': [(x,G[i],G[i]) for i,x in enumerate(X)],
            'blue':  [(x,B[i],B[i]) for i,x in enumerate(X)],}
    plt.register_cmap(name=name+'_r', data=cdict)


# ==== Build some additional colormaps =========================================
### build colormap variants with linear fade in Y'
from matplotlib._cm import datad
from matplotlib._cm_listed import cmaps as cmaps_listed
cmap_list =  sorted(datad.keys()+cmaps_listed.keys())
for cmap_str in cmap_list:
    build_faded_cmap(cmap_str)
### helix colormaps (those are cubehelix variations)
build_helixmap('mrcubehelix', rotations=-1.)
build_helixmap('denshelix', start=.35, rotations=5./3.)
build_helixmap('prismhelix', start=.7, rotations=-5./3.)
build_helixmap('bluehelix', start=0., rotations=0.)
build_helixmap('redhelix', start=1., rotations=0.)
build_helixmap('orange_x', start=.7, rotations=.25)
build_helixmap('greenhelix', start=2., rotations=0.)
build_helixmap('glowhelix', start=.35, rotations=1.0, sat=.75)
build_helixmap('skinhelix', start=2.1, rotations=1.0, sat=1.0)
### digihelix colormaps (similar to cubehelix-style maps, but maximized saturation)
build_shelixmap('cubeshelix')
build_shelixmap('digidens', start=2.5, rotations=+1.5/3.)
build_shelixmap('digilight', start=1., rotations=-1.5/3.)
build_shelixmap('digicube', start=1., rotations=-4./3., sat=1.)
build_shelixmap('digimale', start=2.25, rotations=5./6., sat=1.)
build_shelixmap('digifemale', start=1.25, rotations=5./6., sat=1.)
build_shelixmap('digimic', start=.5, rotations=-1., sat=1.)
build_shelixmap('digired', start=.16, rotations=-.32)
build_shelixmap('digigreen', start=1.16, rotations=-.32)
build_shelixmap('digiblue', start=2.16, rotations=-.32)

