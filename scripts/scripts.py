# ******************** Package dependencies *********************** #


# Checks if all necessary python packages are installed
def check_dependencies(script_folder):
	import imp
	import os
	from distutils.version import LooseVersion

	cpath = os.getcwd()
	packages = ['mpmath', 'numpy', 'setuptools', 'h5py', 'distribute', 'cython', 'scipy', 'yt', 'astropy']
	for pack in packages:
		try:
			imp.find_module(pack)
			print('Package %s already installed.' % pack)
			if pack == 'numpy':
				import numpy
				if LooseVersion(vstring=numpy.__version__) < LooseVersion(vstring='1.10.4'):
					print('Version of package numpy too old. Needs 1.10.4 or higher.')
					raise ImportError
			if pack == 'setuptools':
				import setuptools
				if LooseVersion(vstring=setuptools.__version__) < LooseVersion(vstring='30.0.0'):
					print('Version of package setuptools too old. Needs 30.0.0 or higher.')
					raise ImportError
		except ImportError:
			print('Package %s not found. Installing %s.' % (pack, pack))
			os.chdir(os.path.join(script_folder, 'libraries', pack))
			os.system('python setup.py install --user')
			os.chdir(cpath)

# ***************** Create necessary input files ****************** #


# Adds a higher temperature to the LAMDA files by copying the last collision rate.
def add_high_temp(fpath, dpath):
	import numpy as np

	output = fpath
	output = output.split('/')
	output = output[-1]

	print('Adding higher temperature to the LAMDA file %s.' % output)

	temp = '  1e30'

	# Import source file.
	with open(fpath, 'r') as f:
		read_data = f.read()

	# Create array of data string seperated by lines.
	read_data = read_data.split('\n')

	# Define length of array.
	length = np.size(read_data)

	# Read out number of radiative transmissions. Not needed anymore.
	# rad_trans = int(read_data[read_data.index('!NUMBER OF RADIATIVE TRANSITIONS')+1])
	# nrad = int(0.5 * rad_trans * (rad_trans + 1))

	# Determine number of collision partners.
	n_coll_partner = int(read_data[read_data.index('!NUMBER OF COLL PARTNERS')+1])

	# Create start index of collision parts.
	index_temp = [0]

	for i in range(n_coll_partner):
		# Find next entry of collision temperature and add to our index array.
		index_temp.append(read_data[index_temp[i]:length].index('!COLL TEMPS')+index_temp[i]+1)

		# Find number of collision transition or number of values to changed/added.
		nrad = int(read_data[index_temp[i+1]-4])

		# Find number of collsion temperatures and increase by 1.
		read_data[index_temp[i+1]-2] = str(int(read_data[index_temp[i+1]-2])+1)

		# Add new collision temp to existing collision temperatures.
		read_data[index_temp[i+1]] = read_data[index_temp[i+1]] + temp

		# Read last collision rate for all transitions nrad and add it again at the end.
		for j in range(index_temp[i+1]+2, index_temp[i+1]+2+nrad):
			tmp = ' ' + read_data[j].split()[-1]
			read_data[j] = read_data[j] + tmp

	# Write file at destination path dpath.
	with open(dpath + '/' + output, 'w+') as f:
		for i in range(length-1):
			f.write(read_data[i] + '\n')


# Creates the camera_wavelength_micron.inp file from a source frequency and a velocity width.
def make_wavelength_file(lam, delv, nlam):
	import numpy as np
	import scipy.constants as const

	# Define resting wavelength lam_0 in mum, velocity width in one direction delv in km/s and
	# the number of velocity channels n.

	# Set nlam to odd number to ensure central bin for source wavelength.
	if nlam % 2 == 0:
		nlam = nlam + 1

	print('Creating the camera_wavelenght_micron.inp file around %s micron with +/- %s km/s in %s bins' % (
			np.round(lam * 1e6, 1), delv, nlam))

	# Define name of output file
	output = 'camera_wavelength_micron.inp'

	# Increases number of decimal places for central wavelength lam_s of the spectrum
	lam_s = np.double(lam)

	# Calculate upper and lower wavelength caused by doppler shift from the velocity.
	beta = np.double(delv) * 1e3/const.c
	lam_low = lam_s * np.sqrt((1-beta)/(1+beta))
	lam_high = lam_s * np.sqrt((1+beta)/(1-beta))
	print(lam_low, lam_high)

	# Create linear spaced wavelengths over the spectrum from lam_low to lam_high
	a = np.linspace(lam_low, lam_high, nlam, dtype=np.double)

	# Write output file
	f = open(output, 'w+')
	f.write('%d \n' % nlam)

	for i in range(0, nlam):
		f.write('%.14f \n' % a[i])

	f.close()


# Creates the radmc3d.inp file from the given settings.
def create_radmc_inp(settings):
	print('Creating radmc3d.inp file.')
	output = 'radmc3d.inp'
	with open(output, 'w+') as f:
		for s in settings:
			f.write(s+'\n')


# Creates the lines.inp file from the given settings.
def create_lines_inp(settings):
	print('Creating lines.inp file.')
	output = 'lines.inp'
	with open(output, 'w+') as f:
		for s in settings:
			f.write(s+'\n')


# Creates the number densities files for para and ortho H2, Cp and electrons.
def create_para_ortho(fpath, ftype):
	import numpy as np
	import os

	print('Converting H2 number densities to para- and ortho-H2 number densities')
	cpath = os.getcwd()
	os.chdir(fpath)

	# Load all necessary files to create the number densities for o-h2, p-h2, elec and cp.

	temp = readout_file(fname='gas_temperature', headersize=2, dtype=ftype, ftype='inp')
	ndens_h2 = readout_file(fname='numberdens_h2', headersize=2, dtype=ftype, ftype='inp')

	nr_entries = temp.size

	# Calculate para and ortho densities for H2 from H2 number density and gas temperature.
	ndens_ph2 = ndens_h2/(9.*np.exp(-170.5/temp)+1.)
	ndens_oh2 = ndens_h2 - ndens_ph2

	ratio = np.where(ndens_ph2 > 0, ndens_oh2 / ndens_ph2, 4.)

	ndens_ph2 = np.where(ratio > 3., ndens_h2 * 0.25, ndens_ph2)
	ndens_oh2 = np.where(ratio > 3., ndens_h2 * 0.75, ndens_oh2)

	writeout_file(fname='numberdens_p-h2', header='1\n%s\n' % nr_entries, data=ndens_ph2, dtype=ftype, ftype='inp')
	writeout_file(fname='numberdens_o-h2', header='1\n%s\n' % nr_entries, data=ndens_oh2, dtype=ftype, ftype='inp')

	os.chdir(cpath)


def create_cp_e(fpath, script_folder, threshold, ftype):
	import numpy as np
	import os
	from scipy import interpolate

	print(
		'Modifying Cp number densities with Sutherland & Doptia 1993 corretion and '
		'determine the electron number densities.')

	cpath = os.getcwd()
	os.chdir(fpath)

	temp = readout_file(fname='gas_temperature', headersize=2, dtype=ftype, ftype='inp')
	ndens_hp = readout_file(fname='numberdens_hp', headersize=2, dtype=ftype, ftype='inp')
	ndens_cp = readout_file(fname='numberdens_cp', headersize=2, dtype=ftype, ftype='inp')

	temp_max = np.max(temp)
	nr_entries = temp.size

	# Derive the correction factor according to Sutherland & Dopita 1993
	suther_temp, suther_cp = 10**np.loadtxt(script_folder+'/asc2plot_Cp_Sutherland_forCalculation.txt', unpack=True)
	suther_factor = interpolate.interp1d(suther_temp, suther_cp, kind='linear')
	threshold_max = np.max(suther_temp)

	# Modify the Cp density according to Sutherland & Dopita 1993 in combination with previous defined factor.
	if temp_max > threshold:
		ndens_cp_mod = np.where(temp < threshold_max, ndens_cp, 0.)
		index = np.where((temp >= threshold) & (temp < threshold_max))
		ndens_cp_mod[index] = suther_factor(temp[index]) * ndens_cp[index]
	else:
		ndens_cp_mod = ndens_cp

	ndens_e = ndens_cp_mod + ndens_hp + 2. * (ndens_cp - ndens_cp_mod)

	if ftype == 'ascii':
		os.rename('numberdens_cp.inp', 'numberdens_cp_old.inp')
	elif ftype == 'binary':
		os.rename('numberdens_cp.binp', 'numberdens_cp_old.binp')

	writeout_file(fname='numberdens_cp', header='1\n%s\n' % nr_entries, data=ndens_cp_mod, dtype=ftype, ftype='inp')
	writeout_file(fname='numberdens_e', header='1\n%s\n' % nr_entries, data=ndens_e, dtype=ftype, ftype='inp')

	os.chdir(cpath)


# Creates a microturbulence_%s.inp file for a given molecule from the gas temperature.
def create_microturbulence_mol(fpath, mol_mass, ftype):
	import numpy as np
	import scipy.constants as const
	import os

	cpath = os.getcwd()
	os.chdir(fpath)

	print('Creating microturbulence.inp with mol mass %s' % mol_mass)

	gas_temp = readout_file(fname='gas_temperature', headersize=2, dtype=ftype)

	microturb = 100 * np.sqrt(2 * const.k * gas_temp/(mol_mass * const.proton_mass))
	nr_entries = gas_temp.size

	writeout_file(fname='microturbulence', header='1\n%i\n' % nr_entries, data=microturb, dtype=ftype, ftype='inp')

	os.chdir(cpath)


# Creates the number density files for CO, 13CO and C18O with and without CO freezeout
# depending on the cosmic ray ionization rate.
def create_co(fpath, freezeout, ftype, crir=3.e-17):
	import numpy as np
	import os

	cpath = os.getcwd()
	os.chdir(fpath)

	nd_co = readout_file(fname='numberdens_co', headersize=2, dtype=ftype, ftype='inp')

	co_size = nd_co.size

	# Open the input files for H2, H and the dust temperature.

	gas_temp = readout_file(fname='gas_temperature', headersize=2, dtype=ftype, ftype='inp')
	dust_temp = readout_file(fname='dust_temperature', headersize=3, dtype=ftype, ftype='dat')
	nd_h = readout_file(fname='numberdens_ha', headersize=2, dtype=ftype, ftype='inp')
	nd_h2 = readout_file(fname='numberdens_h2', headersize=2, dtype=ftype, ftype='inp')

	# Create our new output files for 13CO and C18O.
	# Also clear the CO file if we include freezeout.
	# Numbers from freeze-out taken from Seifried et al. 2017, motivated by Glover & Clark 2016
	# The exact number where taken from the publications Herbst & Cuppen 2006 and Hollenbach et al. 2009
	# Note that thermal desorption was added here again (omitted in Seifried et al. 2017)
	# The CRIR is the ionisation rate with respect to atomic hydrogen, i.e. that used in FLASH

	factor = 1
	if freezeout == 1:
		k_cr = 5.7e-13 * crir * 1e17
		k_ads = 3.44e-18 * np.sqrt(gas_temp) * (2 * nd_h2 + nd_h)
		k_therm = 1.04e12 * np.exp(-960./dust_temp)
		factor = (k_cr + k_therm) / (k_cr + k_therm + k_ads)
		if ftype == 'ascii':
			os.rename('numberdens_co.inp', 'numberdens_co_old.inp')
		elif ftype == 'binary':
			os.rename('numberdens_co.binp', 'numberdens_co_old.binp')
		writeout_file(fname='numberdens_co', header='1\n%s\n' % co_size, data=nd_co * factor, dtype=ftype, ftype='inp')

	writeout_file(fname='numberdens_13co', header='1\n%s\n' % co_size, data=nd_co * factor/69., dtype=ftype, ftype='inp')
	writeout_file(fname='numberdens_c18o', header='1\n%s\n' % co_size, data=nd_co * factor/557., dtype=ftype, ftype='inp')

	os.chdir(cpath)


# Creates the He number density with a ratio 1 to 10 in regards to H
def create_he(fpath, ftype):
	nd_h = readout_file(fname='numberdens_ha', headersize=2, dtype=ftype, ftype='inp')
	nd_h2 = readout_file(fname='numberdens_h2', headersize=2, dtype=ftype, ftype='inp')
	nd_hp = readout_file(fname='numberdens_hp', headersize=2, dtype=ftype, ftype='inp')
    
	he_size = nd_h2.size
    
	nd_he = 0.1*(nd_h + nd_hp + 2.*nd_h2)
    
	writeout_file(fname='numberdens_he', header='1\n%s\n' % he_size, data=nd_he, dtype=ftype, ftype='inp')


# Creates the number densities for O+, O atomic and H3+.
# Also corrects the electron number density for the new species.
def create_extra_species(fpath, ftype):
	import numpy as np
	import os

	cpath = os.getcwd()
	os.chdir(fpath)

	# Load all necessary files to create the number densities for o-h2 and p-h2.
	temp = readout_file(fname='gas_temperature', headersize=2, dtype=ftype, ftype='inp')

	ndens_h2 = readout_file(fname='numberdens_h2', headersize=2, dtype=ftype, ftype='inp')
	ndens_hp = readout_file(fname='numberdens_hp', headersize=2, dtype=ftype, ftype='inp')
	ndens_ha = readout_file(fname='numberdens_ha', headersize=2, dtype=ftype, ftype='inp')

	nr_entries = temp.size

	ndens_o = readout_file(fname='numberdens_o', headersize=2, dtype=ftype, ftype='inp')
	ndens_hep = readout_file(fname='numberdens_hep', headersize=2, dtype=ftype, ftype='inp')
	ndens_ohx = readout_file(fname='numberdens_ohx', headersize=2, dtype=ftype, ftype='inp')

	# WRITING DENSITIES OF O+, OI AND H3+. ATTENTION --> I THINK THAT ALL THE CONSTANTS CAN BE WRONG

	cst1 = 6.4e-10
	cst93 = 1.6e-9
	cst75 = 7.2e-15
	cst59 = 1.7e-9
	cst81 = 2.0e-9
	# cst84 = 2e-9
	# cst86 = 8e-10

	num_op = \
		(1.26e-9 * (temp / 1e4) ** 0.517e0 + 4.25e-10 * (temp / 1e4) ** 6.69e-3) * np.exp(-227e0 / temp) * ndens_o * ndens_hp \
		+ 1.35e-9 * (0.62 + 45.41 * np.sqrt(temp)) * ndens_hep * ndens_ohx

	den_op = \
		cst93 * ndens_h2 \
		+ (2.08e-9 * (temp/1e4)**0.405e0 + 1.11e-11 * (temp/1e4)**(-0.458e0)) * ndens_ha \
		+ (1.26e-9 * (temp/1e4)**0.517e0 + 4.25e-10 * (temp/1e4)**6.69e-3) * np.exp(-227e0/temp) * ndens_hp

	ndens_op = num_op/den_op

	del num_op, den_op

	ndens_oa = ndens_o - ndens_op

	ndens_op[ndens_hp < 1.e-59] = 1.e-60
	ndens_oa[ndens_hp < 1.e-59] = 1.e-60

	writeout_file(fname='numberdens_op', header='1\n%s\n' % nr_entries, data=ndens_op, dtype=ftype, ftype='inp')
	writeout_file(fname='numberdens_oa', header='1\n%s\n' % nr_entries, data=ndens_oa, dtype=ftype, ftype='inp')

	del ndens_o, ndens_ohx, ndens_op

	# Load additional files to create number density for elec and cp.
	# ndens_cp = readout_file(fname='numberdens_cp', headersize=2, dtype=ftype, ftype='inp')
	ndens_e = readout_file(fname='numberdens_e', headersize=2, dtype=ftype, ftype='inp')
	ndens_mp = readout_file(fname='numberdens_mp', headersize=2, dtype=ftype, ftype='inp')
	ndens_hco = readout_file(fname='numberdens_hco', headersize=2, dtype=ftype, ftype='inp')

	ndens_e = ndens_e + ndens_hep + ndens_hco + ndens_mp

	del ndens_hco

	ch30 = np.where(
		temp < 6.17e2,
		1.e-8,
		1.32e-6 / (temp ** 0.76e0)
	)

	ch105 = 2.24e-9 * ((temp/3e2)**0.042e0) * np.exp(-temp/4.66e4)

	fh3p = ch105 * ndens_h2 / (ch105 * ndens_h2 + cst1 * ndens_ha + ch30 * ndens_e)

	del ch105, ch30

	ch24 = np.where(
		temp < 3.e4,
		3.7289e-10*np.exp(-2.1237150e4/temp),
		(
			-3.3232183e-7
			+ 3.3735382e-7 * np.log(temp)
			- 1.4491368e-7 * (np.log(temp) ** 2)
			+ 3.4172805e-8 * (np.log(temp) ** 3)
			- 4.7813720e-9 * (np.log(temp) ** 4)
			+ 3.9731542e-10 * (np.log(temp) ** 5)
			- 1.8171411e-11 * (np.log(temp) ** 6)
			+ 3.5311932e-13 * (np.log(temp) ** 7)
		) * np.exp(-2.1237150e4 / temp)
	)

	cosmic_ray_ion_rate = 3e-17
	cr2 = 2e0 * cosmic_ray_ion_rate

	num_h3p = fh3p * (
			(cr2 / (ndens_ha + ndens_h2 + ndens_hp)) * ndens_h2
			+ cst75 * ndens_hep * ndens_h2 + ch24 * ndens_hp * ndens_h2
	)

	del ch24, fh3p
	del ndens_h2, ndens_ha, ndens_hep

	ch106 = 6.7e-8 / ((temp/3e2)**0.52e0)
	ch153 = 1.99e-9 * temp**(-0.19e0)
	ch159 = 1.04e-9 * (temp/3e2)**(-0.00231)\
		+ (temp**1.5e0) * (
			3.4e-8 * np.exp(-7.62e0/temp)
			+ 6.97e-9 * np.exp(-1.38e0/temp)
			+ 1.31e-7 * np.exp(-2.66e1/temp)
			+ 1.51e-4 * np.exp(-8.11e3/temp)
			)

	ndens_c = readout_file(fname='numberdens_c', headersize=2, dtype=ftype, ftype='inp')
	ndens_co = readout_file(fname='numberdens_co', headersize=2, dtype=ftype, ftype='inp')

	den_h3p = ch159 * ndens_c + ch153 * ndens_oa + cst59 * ndens_co + ch106 * ndens_e + cst81 * ndens_mp

	del ndens_mp, ndens_co, ndens_oa, ndens_c
	del ch106, ch153, ch159

	ndens_h3p = num_h3p/den_h3p

	del num_h3p, den_h3p

	ndens_e = ndens_e + ndens_h3p

	ndens_h3p[ndens_hp < 1.e-59] = 1.e-60
	ndens_e[ndens_hp < 1.e-59] = 1.e-60

	del ndens_hp

	writeout_file(fname='numberdens_h3p', header='1\n%s\n' % nr_entries, data=ndens_h3p, dtype=ftype, ftype='inp')
	writeout_file(fname='numberdens_e', header='1\n%s\n' % nr_entries, data=ndens_e, dtype=ftype, ftype='inp')

	os.chdir(cpath)


# Creates the number density input file for 13Cp
def convert_13cp(fpath, ftype):
	import os

	cpath = os.getcwd()
	os.chdir(fpath)

	ndens_cp = readout_file(fname='numberdens_cp', headersize=2, dtype=ftype, ftype='inp')

	# Get number of entries as it is needed to create the file header.
	nr_entries = ndens_cp.size

	# Calculate number density of 13Cp
	ndens_13cp = ndens_cp * 0.625/67.

	writeout_file(fname='numberdens_13cp', header='1\n%s\n' % nr_entries, data=ndens_13cp, dtype=ftype, ftype='inp')

	os.chdir(cpath)


# Creates the gas_spintemperature.inp file with or without including the Wouthuysen-Field effect.
def create_spintemp_wf(fpath, wf, ftype):
	import numpy as np
	import scipy.constants as const
	import os
	import sys

	cpath = os.getcwd()
	os.chdir(fpath)

	temp = readout_file(fname='gas_temperature', headersize=2, dtype=ftype, ftype='inp')
	ndens_ha = readout_file(fname='numberdens_ha', headersize=2, dtype=ftype, ftype='inp')
	ndens_e = readout_file(fname='numberdens_e', headersize=2, dtype=ftype, ftype='inp')

	# Central line frequency for HI taken from Kim, Ostriker et al. 2014
	nu = 1.4204e9

	# Einstein coefficient of the line taken from Gould 1994
	a10 = 2.8843e-15

	# Temperature of the background 21 cm radiation field
	tr = 3.77

	# Lyman alpha photon nunber density
	nalpha = 1.e-6

	# Plank and Boltzmann constants with converted energies to erg.
	h = const.h/const.erg
	kb = const.k/const.erg

	# Temperature corresponding to the wavelength???
	t0 = h * nu / kb

	# Number of entries needed for header of the files.
	nr_entries = temp.size

	t2 = temp/100.

	# Derive factor from the temperature for hydrogen and electrons:
	# For H we set all gas below 300 K on the value and also all gas above on the other instead of limiting the ranges
	# to 20 - 300 K and 300 - 1000 K.
	k10 = np.where(
		temp < 300., 1.19e-10 * t2 ** (0.74 - 0.20*np.log(t2)), 2.24e-10 * t2 ** 0.207 * np.e ** (-0.876 / t2)
	)

	k10e = np.where(temp <= 1350., 2.26e-9 * np.sqrt(t2), 2.26e-9 * np.sqrt(1350./100))

	# Rate of downward transition by collisions.
	rc10 = ndens_ha * k10 + ndens_e * k10e

	# Transition probabilites via collisions or Ly alpha photons.
	yc = t0/temp * rc10/a10
	yalpha = 5.9e11 * nalpha/(temp**1.5)

	# Calculate the spin temperature with or without including the Wouthuysen-Field effect.
	if wf == 1:
		number_ts = (tr + yc*temp + yalpha*temp) / (1 + yc + yalpha)
	elif wf == 0:
		number_ts = (tr + yc*temp) / (1 + yc)
	else:
		sys.exit('Please specify whether you want to include the Wouthuysen-Field effect.')

	writeout_file(fname='gas_spintemperature', header='1\n%s\n' % nr_entries, data=number_ts, dtype=ftype, ftype='inp')

	os.chdir(cpath)


# Creates dust_density.inp file with a given dust to gas ratio
def create_dust_dens(fpath, dust_to_gas, ftype):
	import os

	cpath = os.getcwd()
	os.chdir(fpath)

	# Load gas density
	dens = readout_file(fname='gas_density', headersize=2, dtype=ftype, ftype='inp')

	# Count the number of components. Needed for header of dust density file.
	number_entries = dens.size
	dens = dens * dust_to_gas

	writeout_file(fname='dust_density', header='1\n%s\n1\n' % number_entries, data=dens, dtype=ftype, ftype='inp')

	os.chdir(cpath)


# ************************** octtree routine ************************** #


def silcc2radmc(fname, dpath, chemnet, ftype, coords=None):
	import os
	import sys
	import numpy as np
	import scipy.constants as const

	# Get path to folder
	cpath = os.getcwd()
	os.chdir(dpath)

	if chemnet == 'NL97':
		nint = 2
		nfloat = 17
	elif chemnet == 'NL99':
		nint = 2
		nfloat = 26
	else:
		sys.exit('Please specify the chemical network to either NL97 or NL99')

	# i0 = leaf
	# i1 = level
	# f0 = xcoord
	# f1 = ycoord
	# f2 = zcoord
	# f3 = gas density
	# f4 = gas temperature
	# f5 = dust temperature
	# f6 = magnetic field x
	# f7 = magnetic field y
	# f8 = magnetic field z
	# f9 = gas velocity x
	# f10 = gas velocity y
	# f11 = gas velocity z
	# f12 = H2 abundance fraction

	# if 'NL97':
		# f13 = Ha abundance fraction
		# f14 = Hp abundance fraction
		# f15 = CO abundance fraction
		# f16 = Cp abundance fraction

	# elif 'NL99'
		# f13 = Hp abundance fraction
		# f14 = Ha abundance fraction
		# f15 = CO abundance fraction
		# f16 = C abundance fraction
		# f17 = Cp  abundance fraction
		# f18 = He abundance fraction
		# f19 = Hep abundance fraction
		# f20 = O abundance fraction
		# f21 = CHx abundance fraction
		# f22 = OHx abundance fraction
		# f23 = HCO abundance fraction
		# f24 = M abundance fraction
		# f25 = Mp abundance fraction

	dt = np.dtype([('i%i' % i, np.int32) for i in range(nint)] + [('f%i' % i, np.float32) for i in range(nfloat)])
	dt_head = np.dtype(
		[('i%i' % i, np.float32) for i in range(1)]
		+ [('i%i' % i, np.int32) for i in range(1, nint)]
		+ [('f%i' % i, np.float32) for i in range(nfloat)])

	with open(fname, 'rb') as f:
		data_head = np.fromfile(f, dtype=dt_head, count=1)
		data = np.fromfile(f, dtype=dt)

	data['i0'] = np.where(data['i0'] == 1, 0, 1)

	# Counter for number of cells. Needed for header of files.
	leafs = data['i0'] == 0
	truecells = data['i0'][leafs].size

	psize = data_head['i0'][0]/2.
	headergas = '1\n%i\n' % truecells
	headerdust = '1\n%i\n1\n' % truecells
	headeramr = '1\n' + '1\n' + '1\n' + '0\n' + '1\t1\t1\n' + '1\t1\t1\n'
	headeramr += '%i\t%i\t%i\n' % (data['i1'].max(), truecells, data['i1'].size)
	headeramr += '%1.8e\t%1.8e\n' % (-psize, psize) + '%1.8e\t%1.8e\n' % (-psize, psize)
	headeramr += '%1.8e\t%1.8e\n' % (-psize, psize)

	if chemnet == 'NL97':
		# See Annika Franeck Doctor Thesis page 76.
		ab_he = 0.1
		ab_c = 1.4e-4
		ab_o = 3.2e-4
		mu_he = 4.002602
		mu_c = 12.011
		mu_o = 15.9994
		normalization = 1. + ab_he * mu_he + ab_c * mu_c + ab_o * mu_o
		mu_co = 12.

	elif chemnet == 'NL99':
		normalization = 1.0
		mu_co = 28.

	else:
		sys.exit('Set chemical network to either NL97 or NL99')

	if len(coords) == 0:
		x1 = - np.inf
		x2 = np.inf
		y1 = - np.inf
		y2 = np.inf
		z1 = - np.inf
		z2 = np.inf

	elif len(coords) == 9:
		x1 = coords[3] - coords[0]
		x2 = coords[4] - coords[0]
		y1 = coords[5] - coords[1]
		y2 = coords[6] - coords[1]
		z1 = coords[7] - coords[2]
		z2 = coords[8] - coords[2]

	else:
		sys.exit('check coordinates')

	dxhalf = data_head['i0'][0] / (2. ** data_head['i1'][0]) / 2.
	cell_out = np.any(
		np.concatenate((
			np.asarray([data['f0'], data['f1'], data['f2']]).T + dxhalf < np.asarray([x1, y1, z1]),
			np.asarray([data['f0'], data['f1'], data['f2']]).T - dxhalf > np.asarray([x2, y2, z2])),
			axis=1), axis=1)

	am = const.atomic_mass/const.gram
	# kb = const.k/const.erg

	writeout_file(fname='amr_grid', header=headeramr, data=data['i0'], dtype=ftype, ftype='inp')

	temp = data['f3']
	temp[cell_out] = 1.e-60
	writeout_file(fname='gas_density', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

	temp = data['f4']
	writeout_file(fname='gas_temperature', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

	temp = data['f5']
	writeout_file(fname='dust_temperature', header=headerdust, data=temp[leafs], dtype=ftype, ftype='dat')

	temp = np.asarray([data['f9'], data['f10'], data['f11']]).T
	writeout_file(fname='gas_velocity', header=headergas, data=temp[leafs].T, dtype=ftype, ftype='inp')

	temp = data['f12'] * data['f3'] / normalization / (2 * am)
	temp[cell_out] = 1.e-60
	writeout_file(fname='numberdens_h2', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

	if chemnet == 'NL97':

		temp = data['f13'] * data['f3'] / normalization / (1 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_ha', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f14'] * data['f3'] / normalization / (1 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_hp', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f15'] * data['f3'] / normalization / (mu_co * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_co', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f16'] * data['f3'] / normalization / (12 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_cp', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

	# The order of c <--> cp and ha <--> hp is swapped.
	# Reassure that in the map_FLASH_data.cpp file the order is identical.
	if chemnet == 'NL99':

		temp = data['f13'] * data['f3'] / normalization / (1 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_hp', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f14'] * data['f3'] / normalization / (1 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_ha', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f15'] * data['f3'] / normalization / (mu_co * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_co', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f16'] * data['f3'] / normalization / (12 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_c', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f17'] * data['f3'] / normalization / (12 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_cp', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f18'] * data['f3'] / normalization / (4 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_he', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f19'] * data['f3'] / normalization / (4 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_hep', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f20'] * data['f3'] / normalization / (16 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_o', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f21'] * data['f3'] / normalization / (13 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_chx', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f22'] * data['f3'] / normalization / (17 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_ohx', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f23'] * data['f3'] / normalization / (29 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_hco', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f24'] * data['f3'] / normalization / (28 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_m', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

		temp = data['f25'] * data['f3'] / normalization / (28 * am)
		temp[cell_out] = 1.e-60
		writeout_file(fname='numberdens_mp', header=headergas, data=temp[leafs], dtype=ftype, ftype='inp')

	os.chdir(cpath)


# *************************** hdf5 routine old *************************** #

# Creates field for gas density for yt.
def _dens(field, data):
	return data["dens"]


# Creates field for dust density for yt. dust_to_gas has to be defined as global variable.
# Currently not needed as we calculate the dust density afterwards with numpy to save computational time.
def _dust_density(field, data):
	return dust_to_gas*data["dens"]


# Creates field for gas temperature for yt.
def _temp_gas(field, data):
	return data["temp"]


# Creates field for dust temperature for yt.
def _dust_temperature(field, data):
	return data["tdus"]


# Creates field for number density of ionized hydrogen for yt. au and mu_h have to be defined as global variable.
def _number_density_hp(field, data):
	return data["ihp "]*data["dens"]/(normalization*au*mu_h)


# Creates field for number density of atomic hydrogen for yt. au and mu_h have to be defined as global variable.
def _number_density_ha(field, data):
	return data["iha "]*data["dens"]/(normalization*au*mu_h)


# Creates field for number density of molecular hydrogen for yt. au and mu_h2 have to be defined as global variable.
def _number_density_h2(field, data):
	return data["ih2 "]*data["dens"]/(normalization*au*mu_h2)


# Creates field for number density of carbon monoxide for yt. au, mu_co and normalization have to be defined
# as global variable. Normalization is needed for NL97 with a value of about 1.4.
def _number_density_co(field, data):
	return data["ico "]*data["dens"]/(normalization*au*mu_co)


# Creates field for number density of ionized carbon for yt. au and mu_c have to be defined as global variable.
def _number_density_cp(field, data):
	return data["icp "]*data["dens"]/(normalization*au*mu_c)


# Creates field for number density of atomic hydrogen for yt. au and mu_h have to be defined as global variable.
def _number_density_ca(field, data):
	return data["ic  "]*data["dens"]/(normalization*au*mu_c)


# Creates field for microtubulence for yt. au, mu and k have to be defined as global variable.
# Currently not needed as we calculate the microturblence afterwards with numpy to save computational time.
def _microturbulence_transonic(field, data):
	return (2.0*k*data["temp"]/(mu*au)) ** 0.5


# Creates field for microtubulence for yt. au, mu_c and k have to be defined as global variable.
# Currently not needed as we calculate the microturblence afterwards with numpy to save computational time.
def _microturbulence_cp(field, data):
	return (2.0*k*data["temp"]/(mu_c*au)) ** 0.5


# Creates field for visual extinction for yt.
def _av(field, data):
	return data["cdto"]


# Converts unit strings to cgs units. Currently not needed.
def yt_unit_conv(string):
	s = string.replace('code_temperature', 'K')
	s = s.replace('code_mass', 'g')
	s = s.replace('code_length', 'cm')
	s = s.replace('code_energy', 'erg')
	s = s.replace('code_time', 's')
	s = s.replace('code_magnetic', 'gauss')
	if s == '':
		s = 'dimensionless'
	return s


# Extracts all needed data from the HDF5 files.
def yt2radmc(fname, dpath, chemnet):
	# Will need yt installation.
	import yt.mods as ytmods
	import yt.analysis_modules.radmc3d_export.api as ytapi
	import scipy.constants as const
	import sys
	import os

	cpath = os.getcwd()
	# Switch to destination path
	os.chdir(dpath)

	# Load HDF5 file
	pf = ytmods.load(fname)
	pf.create_field_info()

	# Set global variables for all the functions which extract the data
	global dust_to_gas, au, mu_h, mu_h2, mu_c, normalization, mu_co, k, mu

	dust_to_gas = 0.01
	au = const.atomic_mass/const.gram  # * pf.mass_unit
	mu_h = 1.00
	mu_h2 = 2 * mu_h
	mu_c = 12.0
	k = const.k/const.erg  # * pf.mass_unit * pf.length_unit**2 / pf.time_unit**2
	mu = 2.3

	# Different normalization factors for the chemical networks of Nelson and Lager
	if chemnet == 'NL97':
		# See Annika Franeck Doctor Thesis page 76 for nomalization factor.
		ab_he = 0.1
		ab_c = 1.4e-4
		ab_o = 3.2e-4
		mu_he = 4.002602
		mu_c = 12.011
		mu_o = 15.9994
		normalization = 1. + ab_he * mu_he + ab_c * mu_c + ab_o * mu_o
		mu_co = 12.

	elif chemnet == 'NL99':
		normalization = 1.0
		mu_co = 28.

	else:
		sys.exit('Set chemical network to either NL97 or NL99.')

	# Go through all fields and check if they are available.
	# Also a sanity check for mandatory fields such as density
	# Some fields which are not extracted commented out are created later using python (mainly numpy)
	# to save computational time.

	not_avail = ['dens', 'temp', 'tdus', 'ihp ', 'iha ', 'ih2 ', 'ico ', 'icp ', 'ic  ', 'cdto']
	fields = dir(pf.fields.flash)

	# Adds field for gas denstity. Dust density is skipped as mentioned before.
	if 'dens' in fields:
		unit_dens = pf.field_info.get(('flash', 'dens')).units

		pf.add_field("Density", function=_dens, units=unit_dens, sampling_type='cell')
		# pf.add_field("DustDensity", function=_DustDensity, units=unit_dens, sampling_type='cell')

		not_avail.remove('dens')

	else:
		sys.exit('Density not defined or has different name.')

	# Adds field for temperature. Microturbulence is skipped as mentioned before.
	if 'temp' in fields:
		unit_temp = pf.field_info.get(('flash', 'temp')).units
		# unit_turb = 'sqrt(' + pf.field_info.get(('flash', 'temp')).units + ')'

		pf.add_field("GasTemperature", function=_temp_gas, units=unit_temp, sampling_type='cell')
		# pf.add_field("Microturbulence_transonic", function=_microturbulence_transonic,
		# units=unit_turb, sampling_type='cell')
		# pf.add_field("Microturbulence_Cp", function=_microturbulence_cp, units=unit_turb, sampling_type='cell')

		not_avail.remove('temp')

	# Adds field for dust temperature.
	if 'tdus' in fields:
		unit_tdus = pf.field_info.get(('flash', 'tdus')).units

		pf.add_field("DustTemperature", function=_dust_temperature, units=unit_tdus, sampling_type='cell')
		not_avail.remove('tdus')

	# Adds field for number density of ionized hydrogen.
	if 'ihp ' in fields:
		pf.add_field("NumberDensityHp", function=_number_density_hp, units=unit_dens, sampling_type='cell')
		not_avail.remove('ihp ')

	# Adds field for number density of hydrogen.
	if 'iha ' in fields:
		pf.add_field("NumberDensityHa", function=_number_density_ha, units=unit_dens, sampling_type='cell')
		not_avail.remove('iha ')

	# Adds field for number density of molecular hydrogen.
	if 'ih2 ' in fields:
		pf.add_field("NumberDensityH2", function=_number_density_h2, units=unit_dens, sampling_type='cell')
		not_avail.remove('ih2 ')

	# Adds field for number density of carbon monoxide (12C16O).
	if 'ico ' in fields:
		pf.add_field("NumberDensityCO", function=_number_density_co, units=unit_dens, sampling_type='cell')
		not_avail.remove('ico ')

	# Adds field for number density of ionized carbon.
	if 'icp ' in fields:
		pf.add_field("NumberDensityCp", function=_number_density_cp, units=unit_dens, sampling_type='cell')
		not_avail.remove('icp ')

	# Adds field for number density of carbon (12C).
	if 'ic  ' in fields:
		pf.add_field("NumberDensityCa", function=_number_density_ca, units=unit_dens, sampling_type='cell')
		not_avail.remove('ic  ')

	# Adds field for number density of visual extinction.
	if 'cdto' in fields:
		unit_av = pf.field_info.get(('flash', 'cdto')).units
		pf.add_field("extinction", function=_av, units=unit_av, sampling_type='cell')
		not_avail.remove('cdto')

	# Combines velocity components to include all three dimensions.
	velocity_fields = ["velocity_x", "velocity_y", "velocity_z"]

	# Somehow needed as it crashes otherwise.
	pf.periodicity = (True, True, True)

	# Sets the writer to maximum refinement level of the data.
	writer = ytapi.RadMC3DWriter(pf, max_level=pf.max_level)

	# Writes 'amr_grid.inp'.
	writer.write_amr_grid()

	# Writes 'gas_density.inp'.
	if 'dens' not in not_avail:
		writer.write_line_file("Density", "gas_density.inp")
		# writer.write_dust_file("DustDensity", "dust_density.inp")

	# Writes 'dust_temperature.dat'.
	if 'tdus' not in not_avail:
		writer.write_dust_file("DustTemperature", "dust_temperature.dat")

	# Writes 'gas_temperature.inp'.
	if 'temp' not in not_avail:
		writer.write_line_file("GasTemperature", "gas_temperature.inp")
		# writer.write_line_file("Microturbulence_transonic", "microturbulence.inp")
		# writer.write_line_file("Microturbulence_Cp", "microturbulence_cp.inp")

	# Writes 'numberdens_hp.inp'.
	if 'ihp ' not in not_avail:
		writer.write_line_file("NumberDensityHp", "numberdens_hp.inp")

	# Writes 'numberdens_ha.inp'.
	if 'iha ' not in not_avail:
		writer.write_line_file("NumberDensityHa", "numberdens_ha.inp")

	# Writes 'numberdens_h2.inp'.
	if 'ih2 ' not in not_avail:
		writer.write_line_file("NumberDensityH2", "numberdens_h2.inp")

	# Writes 'numberdens_co.inp'.
	if 'ico ' not in not_avail:
		writer.write_line_file("NumberDensityCO", "numberdens_co.inp")

	# Writes 'numberdens_cp.inp'.
	if 'icp ' not in not_avail:
		writer.write_line_file("NumberDensityCp", "numberdens_cp.inp")

	# Writes 'numberdens_ca.inp'.
	if 'ic  ' not in not_avail:
		writer.write_line_file("NumberDensityCa", "numberdens_ca.inp")

	# Writes 'av.inp'.
	if 'cdto' not in not_avail:
		writer.write_line_file("extinction", "av.inp")

	# Writes 'gas_velocity.inp'.
	writer.write_line_file(velocity_fields, "gas_velocity.inp")

	# Return original path and remove global variables to not interfere with any other calculation.
	os.chdir(cpath)
	del dust_to_gas, au, mu_h, mu_h2, mu_c, normalization, mu_co, k, mu


# ****************** FLASH routine ************************* #


# New routine to writeout files in ascii or binary format.
# Header should be a continuous string with \t or \n as separators.
# fname is just the name without the path and file ending.
def writeout_file(fname, header, data, fpath='./', dtype='ascii', ftype='inp'):
	import numpy as np
	import os

	cpath = os.getcwd()
	os.chdir(fpath)

	data = np.asarray(data)

	if dtype == 'ascii':
		with open(fname + '.' + ftype, 'w+') as f:
			f.write(header)
			if data.ndim > 1:
				fmt = '\t'.join(['%1.8e'] * data.shape[0])
				np.savetxt(X=data.T, fname=f, fmt=fmt, comments='')
			else:
				if fname == 'amr_grid':
					data.tofile(f, sep='\n', format='%1i')
				else:
					data.tofile(f, sep='\n', format='%1.8e')
	elif dtype == 'binary':
		with open(fname + '.b' + ftype, 'wb+') as f:
			header = header.replace('\t', '\n')
			header = list(filter(None, header.split('\n')))
			if data.ndim > 1:
				data = data.T.flatten()
			if fname == 'amr_grid':
				header = np.asarray([eval(it) for it in header], dtype=np.float64)
				header[:13].astype(np.int64).tofile(f)
				header[13:].astype(np.float64).tofile(f)
				data.astype(np.int8).tofile(f)
			else:
				header.insert(1, '8')
				header = np.asarray([eval(it) for it in header], dtype=np.int64)
				header.astype(np.int64).tofile(f)
				data.astype(np.float64).tofile(f)
	os.chdir(cpath)


# New routine to readin files in ascii or binary format.
# return_head = True gives both the data and header.
# fname is just the name without the path and file ending.
def readout_file(fname, headersize, fpath='./', dtype='ascii', return_head=False, ftype='inp'):
	import numpy as np
	import sys
	import os

	cpath = os.getcwd()
	os.chdir(fpath)

	if dtype == 'ascii':
		with open(fname + '.' + ftype, 'r') as f:
			data = f.readlines()
			data = np.asarray(data)
			header = data[:headersize]
			data = np.char.strip(data[headersize:]).astype(np.float64)
			data = data

	elif dtype == 'binary':
		with open(fname + '.b' + ftype, 'rb') as f:
			if fname == 'amr_grid':
				header = np.fromfile(f, dtype=np.int64, count=13)
				header2 = np.fromfile(f, dtype=np.float64, count=6)
				header = np.concatenate((header, header2))
				data = np.fromfile(f, dtype=np.int8)
			else:
				header = np.fromfile(f, dtype=np.int64, count=headersize+1)
				if header[1] == 4:
					data = np.fromfile(f, dtype=np.float32)
				elif header[1] == 8:
					data = np.fromfile(f, dtype=np.float64)
				else:
					sys.exit(
						'Unknown dtype for data. Should be either 4 (for 4 bytes) or 8 (for 8 bytes).\n'
						'Was value: %s' % header[1]
					)

	os.chdir(cpath)
	if return_head:
		return data, header
	else:
		return data


# Find all blocks at lowest refinement level which are correspoding to our initial guess
def find_blocks(
		block_list, min_ref_lvl, max_ref_lvl, brlvl, coords, block_size, bsmin, refine_level, gid, center, cuboid=False
):
	import numpy as np
	# import sys

	# For all block which are higher than our set refinement level find the lowest parent which is fulfills our
	# requested refinement level.
	blist_ref = []
	for block in block_list[brlvl > min_ref_lvl]:
		b_tmp = block
		while refine_level[b_tmp] > min_ref_lvl:
			b_tmp = gid[b_tmp][6] - 1
		blist_ref.append(b_tmp)

	# As there will be many overlaps we create a unique list of block ids
	blist_ref = np.unique(blist_ref)
	# And add them to all the cells which already were at the lowest refinement level.
	minref_blist = np.unique(np.concatenate((block_list[brlvl == min_ref_lvl], blist_ref))).astype(int)

	# Check how many blocks fit into the current range in x, y and z at the lowest refinement level.
	bnx = np.round((np.amax(
		coords[minref_blist, 0] + 0.5 * block_size[minref_blist, 0])
		- np.amin(coords[minref_blist, 0] - 0.5 * block_size[minref_blist, 0])
		)/(bsmin * 2**(max_ref_lvl-min_ref_lvl)))

	bny = np.round((np.amax(
		coords[minref_blist, 1] + 0.5 * block_size[minref_blist, 1])
		- np.amin(coords[minref_blist, 1] - 0.5 * block_size[minref_blist, 1])
		)/(bsmin * 2**(max_ref_lvl-min_ref_lvl)))

	bnz = np.round((np.amax(
		coords[minref_blist, 2] + 0.5 * block_size[minref_blist, 2])
		- np.amin(coords[minref_blist, 2] - 0.5 * block_size[minref_blist, 2])
		)/(bsmin * 2**(max_ref_lvl-min_ref_lvl)))

	# Calculate the max number of blocks at lowest refinement level needed to be able to create an amr tree.
	bn = np.max([bnx, bny, bnz])
	bnmax = 2**np.ceil(np.log2(bn))

	# Check if we have all our blocks fill the cuboid at least in numbers. Let's hope this is never the case.
	# Currently no idea how to fix this.
	if not bnx * bny * bnz == minref_blist.shape[0]:
		print('Blocks in x: ', bnx)
		print('Blocks in y: ', bny)
		print('Blocks in z: ', bnz)
		print(minref_blist.shape)
		return 1, 1, 1, 1, 2
		# sys.exit('Could not find enough blocks to fill cuboid.')
	elif cuboid:
		return minref_blist, bnx, bny, bnz, bnmax

	# For each round add blocks at at least one side to reach the maximum number of blocks for amr tree.
	for i in range(int(bnmax - np.min([bnx, bny, bnz]) + 2)):

		minref_blist, bnx, bny, bnz = add_blocks(
			minref_blist=minref_blist, coords=coords, block_size=block_size, gid=gid, bnx=bnx, bny=bny, bnz=bnz,
			bnmax=bnmax, center=center, axis=0)

		minref_blist, bnx, bny, bnz = add_blocks(
			minref_blist=minref_blist, coords=coords, block_size=block_size, gid=gid, bnx=bnx, bny=bny, bnz=bnz,
			bnmax=bnmax, center=center, axis=1)

		minref_blist, bnx, bny, bnz = add_blocks(
			minref_blist=minref_blist, coords=coords, block_size=block_size, gid=gid, bnx=bnx, bny=bny, bnz=bnz,
			bnmax=bnmax, center=center, axis=2)

	return minref_blist, bnx, bny, bnz, bnmax


def add_blocks(minref_blist, coords, block_size, gid, bnx, bny, bnz, bnmax, center, axis):
	import numpy as np

	bn = np.asarray([bnx, bny, bnz])
	gid_ind = 2 * axis

	# Check if there are neghbours availabe for all blocks in + and - direction of the given axis.
	nb_avail_p = np.all(
		coords[
			gid[minref_blist[coords[minref_blist, axis] == coords[minref_blist, axis].max()], gid_ind + 1] - 1, axis
		] > coords[minref_blist, axis].max()
	)

	if nb_avail_p:
		if np.all(gid[minref_blist[coords[minref_blist, axis] == coords[minref_blist, axis].max()], gid_ind + 1] - 1 < 0):
			nb_avail_p = False

	nb_avail_m = np.all(
		coords[
			gid[minref_blist[coords[minref_blist, axis] == coords[minref_blist, axis].min()], gid_ind] - 1, axis
		] < coords[minref_blist, axis].min()
	)

	if nb_avail_m:
		if np.all(gid[minref_blist[coords[minref_blist, axis] == coords[minref_blist, axis].min()], gid_ind] - 1 < 0):
			nb_avail_m = False

	new_b_p = []
	new_b_m = []
	# Check if the maximum number of blocks at lowest ref. level are reached and if the difference greater than 1.
	# In that case we add blocks at either side, if available, otherwise we only add blocks at one side and check
	# which keeps the box center the closest to the given center.
	if bn[axis] != bnmax and bnmax - bn[axis] >= 2:
		if nb_avail_p:
			new_b_p = gid[minref_blist[coords[minref_blist, axis] == coords[minref_blist, axis].max()], gid_ind + 1] - 1
			new_b_p = new_b_p[new_b_p > 0]
			if new_b_p.size == int(np.prod(bn)/bn[axis]):
				bn[axis] += 1
			else:
				new_b_p = []
		if nb_avail_m:
			new_b_m = gid[minref_blist[coords[minref_blist, axis] == coords[minref_blist, axis].min()], gid_ind] - 1
			new_b_m = new_b_m[new_b_m > 0]
			if new_b_m.size == int(np.prod(bn)/bn[axis]):
				bn[axis] += 1
			else:
				new_b_m = []
	elif bn[axis] != bnmax:
		if nb_avail_p and nb_avail_m:
			trig = False
			new_b_p = gid[minref_blist[coords[minref_blist, axis] == coords[minref_blist, axis].max()], gid_ind + 1] - 1
			new_b_p = new_b_p[new_b_p > 0]
			new_b_m = gid[minref_blist[coords[minref_blist, axis] == coords[minref_blist, axis].min()], gid_ind] - 1
			new_b_m = new_b_m[new_b_m > 0]
			boundariesl = np.min(coords[new_b_m] - 0.5 * block_size[new_b_m], axis=axis)[axis]
			boundariesr = np.max(coords[new_b_p] + 0.5 * block_size[new_b_p], axis=axis)[axis]
			if np.abs(boundariesl - center[axis]) < np.abs(boundariesr - center[axis]):
				if new_b_m.size == int(np.prod(bn)/bn[axis]):
					new_b_p = []
					bn[axis] += 1
				else:
					new_b_m = []
					trig = True
			if np.abs(boundariesl - center[axis]) >= np.abs(boundariesr - center[axis]) or trig:
				if new_b_p.shape[axis] == int(np.prod(bn)/bn[axis]):
					new_b_m = []
					bn[axis] += 1
				else:
					new_b_p = []
					new_b_m = []
		elif nb_avail_p:
			new_b_p = gid[minref_blist[coords[minref_blist, axis] == coords[minref_blist, axis].max()], gid_ind + 1] - 1
			new_b_p = new_b_p[new_b_p > 0]
			if new_b_p.size == int(np.prod(bn)/bn[axis]):
				bn[axis] += 1
			else:
				new_b_p = []
		elif nb_avail_m:
			new_b_m = gid[minref_blist[coords[minref_blist, axis] == coords[minref_blist, axis].min()], gid_ind] - 1
			new_b_m = new_b_m[new_b_m > 0]
			if new_b_m.size == int(np.prod(bn)/bn[axis]):
				bn[axis] += 1
			else:
				new_b_m = []

	# Add new blocks to the current list of blocks at lowest refinement level.
	minref_blist = np.unique(np.concatenate((minref_blist, new_b_p, new_b_m))).astype(int)

	return minref_blist, bn[0], bn[1], bn[2]


def blocks_on_grid(b_ids, coords, bnx=0, bny=0, bnz=0):
	import numpy as np

	if bnx == 0 or bny == 0 or bnz == 0:
		bn = 2**(int(np.log2(len(b_ids)))/3)
		bnx = bn
		bny = bn
		bnz = bn

	bgrid = np.reshape(b_ids[np.argsort(a=coords[b_ids, 2])], (int(bnz), int(bnx * bny)))

	for i in range(int(bnz)):
		bgrid[i] = bgrid[i, np.argsort(coords[bgrid[i]][:, 1])]
	bgrid = bgrid.reshape((int(bnz), int(bny), int(bnx)))

	for i in range(int(bnz)):
		for j in range(int(bny)):
			bgrid[i, j] = bgrid[i, j, np.argsort(coords[bgrid[i, j]][:, 0])]

	return bgrid


# Sort all blocks at minimum refinement level and replace block with their higher refinement level counterparts.
def create_blists(minref_blist, max_ref_lvl, block_level, gid, coords, bnx=0, bny=0, bnz=0, cuboid=False):
	import numpy as np
	import sys
	sys.path.insert(0, 'scripts')
	import zorder

	# Put the block of the minimum refinement level on a grid correspoding to their coordinates.
	# Change axes to be in order of x, y and z.
	blist_minsort_tmp = blocks_on_grid(b_ids=minref_blist, coords=coords, bnx=bnx, bny=bny, bnz=bnz).swapaxes(0, 2)
	print('bnx, bny, bnz: ', bnx, bny, bnz)
	print('Block shape: ', blist_minsort_tmp.shape)
	blist_minsort = []
	# Add blocks in amr order to list.
	if cuboid:
		blist_minsort = blist_minsort_tmp.swapaxes(0, 2).flatten()
	else:
		for pos in zorder.zenumerate(blist_minsort_tmp.shape):
			blist_minsort.append(blist_minsort_tmp[pos])

	# Check all blocks if they have children (!-1) using the gid and replace the corresponding block with them.
	maxref_blist = np.asarray(blist_minsort)
	gid_tmp = gid[maxref_blist, 7:]
	tot_nr_blocks = maxref_blist.size
	# Goes through all refinement levels one by one.
	for j in range(max_ref_lvl - block_level + 1):
		# Adds from back to front to circumvent changing the position every time blocks are added.
		for k in reversed(range(len(gid_tmp))):
			if gid_tmp[k, 0] != -1:
				maxref_blist = np.delete(maxref_blist, k)
				maxref_blist = np.insert(maxref_blist, k, gid_tmp[k]-1)
				tot_nr_blocks += 8
		gid_tmp = gid[maxref_blist, 7:]
	return maxref_blist, tot_nr_blocks


def get_max_block_nr(refine_level, block_list, max_ref_lvl, block_lvl):
	import numpy as np

	tot_nr_blocks = 0.
	tm = 0.
	for i in reversed(range(block_lvl + 1, max_ref_lvl + 1)):
		tn = len(np.where(refine_level[block_list] == i)[0])
		tm = (tn + tm)/8.
		tot_nr_blocks += tn + tm

	tot_nr_blocks = int(tot_nr_blocks)
	return tot_nr_blocks


# Currently not used
def build_parent(dens, gid, pf_raw, b_ids):
	import numpy as np

	p_dens = np.zeros_like(dens[b_ids[0]], dtype=np.float32)
	for n in range(8):
		pn = gid[b_ids[n], 6] - 1
		cn = pf_raw['which child'][()][b_ids[n]]
		ti, tj, tk = [(n/4) % 2, (n/2) % 2, n % 2]
		cni, cnj, cnk = [((cn-1)/4) % 2, ((cn-1)/2) % 2, (cn-1) % 2]
		p_dens[4*ti:4*(ti+1), 4*tj:4*(tj+1), 4*tk:4*(tk+1)] = dens[pn][4*cni:4*(cni+1), 4*cnj:4*(cnj+1), 4*cnk:4*(cnk+1)]
	return p_dens


# Create the tree structure of all blocks. This includes spacers for all necessary parent blocks.
def create_full_tree(refine_level, block_list, tot_nr_blocks, max_ref_lvl, min_ref_lvl, cuboid=False):
	import numpy as np

	# Empty for all block ids and parents.
	bid_out = np.zeros(tot_nr_blocks)
	counter = np.zeros(max_ref_lvl+1)

	# We start at the back of the list.
	pos = -1
	# For all blocks, in reverse order we count their refinemnt level and if we reach 8 in any refinement level we
	# leave space for their parent block. Also fill in the block ids at their corresponding position. The block order
	# given by the block_list is very important.
	for j in reversed(block_list):
		bid_out[pos] = j
		counter[refine_level[j]] += 1
		while not np.all(counter != 8):
			for s in np.where(counter == 8):
				counter[s] -= 8
				counter[s-1] += 1
				pos -= 1
			if counter[min_ref_lvl-1] == 1 and cuboid:
				counter = np.zeros(max_ref_lvl+1)
				pos += 1
		pos -= 1
	bid_out = bid_out.astype(int)
	return bid_out


# Currently not used
def find_full_parent(bid, gid, max_ref):
	import numpy as np

	tbid = bid
	for s in range(max_ref):
		pars = np.unique(gid[tbid, 6], return_counts=True)
		for i in np.where(pars[1] == 8)[0]:
			if not np.any(tbid == pars[0][i]-1):
				tbid = np.unique(np.append(tbid, pars[0][i]-1))
	return tbid


# Creates a list of all refinement levels in amr order.
def fill_refine_level(refine_level, block_list, max_ref_lvl, block_lvl, cuboid=False):
	import numpy as np

	r2 = refine_level[block_list]
	r2 -= block_lvl
	# print(refine_level[block_lvl].min(), refine_level[block_list].max())
	# print(r2.min(), r2.max())
	r2[r2 < 0] = 0
	c = 0
	lower_lvl = 0
	if cuboid:
		lower_lvl = 1
	for e in reversed(range(lower_lvl, max_ref_lvl - block_lvl + 1)):
		for d in np.where(r2 == e)[0][::-1]:
			c += 1
			if c == 8:
				r2[d-1] = e-1
				c = 0
	return r2


# Build the gid list of the included blocks from the list of refinemnt levels.
def build_gid(ref_lvl, tot_nr_blocks, max_ref_lvl, block_level, cuboid=False):
	import numpy as np

	# Create a pseudo gid list. We only add the parent and children block ids and ignore the neighbours.
	gid2 = np.zeros((tot_nr_blocks, 15))

	# For each refinement level, starting with the highest, we count form the back up and remember their number until
	# we reach 8. Then we add those as children to the previous block in the list and set the parent accordingly to
	# all children.
	lower_level = 0
	if cuboid:
		lower_level = 1

	for e in reversed(range(lower_level, max_ref_lvl - block_level + 1)):
		count = 0
		# Temporary list of blocks which have the same parent
		tmp_blist = []
		for c in np.where(ref_lvl == e)[0][::-1]:
			count += 1
			tmp_blist.append(c)
			if count == 8:
				gid2[tmp_blist, 6] = c-1
				# Insert flipped values as we count from the back.
				gid2[c-1, 7:] = np.flip(np.asarray(tmp_blist))
				count = 0
				tmp_blist = []

	# Increasing all block ids by one as that is the way FLASH saves them.
	gid2[:, 6:] += 1
	# Changing all leaf values to -1.
	gid2[:, 7:][gid2[:, 7:] == 1] = -1
	# And modify first entry to conform with FLASH notation.
	block_start = ref_lvl == 0
	gid2[block_start, 6] = -1
	return gid2.astype(int)


# Writes out the amr_grid.inp file and returns the number of leaf and branch blocks.
def writeout_amrgrid(gid_list, bounds, mref_lvl, ftype, bx=1, by=1, bz=1, cuboid=False):
	import numpy as np
	from itertools import chain

	# First create the general amr structure from the gid of the blocks which are included in the box.
	# The order of this array is very important at this point.
	# If the blocks have no children (-1) they are leafs (0) otherwise they are branches (1).
	amr = np.zeros_like(gid_list[:, -1])
	amr[gid_list[:, -1] != -1] = 1

	# As each block contains 8**3 cells we need to substitute their refinement structure for the relevant block.
	# Here we create the amr structure of one of those blocks. As they are all the same we can reuse this for all
	# blocks.
	cells_ref = np.asarray([0])
	# There are 3 levels of refinement per block.
	for i in range(3):
		# For each refinement we replace each leaf (0) with a branch (1) and insert 8 leafs behind them.
		for pos in np.where(cells_ref == 0)[0][::-1]:
			cells_ref[pos] = 1
			cells_ref = np.insert(cells_ref, pos+1, np.zeros(8))

	# For each leaf block we insert the above structure and chage them to branch blocks

	amr = [cells_ref if it == 0 else [it] for it in amr]
	amr = np.asarray(list(chain.from_iterable(amr)))

	# The number of leafs and branches can now be counted.
	leaf_nr, br_nr = np.unique(amr, return_counts=True)[1]

	# Creation of the header necessary for RADMC-3D. Most of these are fix values needed for the amr structure.
	# For more information look into the RADMC-3D manual.
	# Only the number of blocks, the maximum refinement level (increased by 3 because we have 3 more refinement
	# levels per block) and the boundaries are needed for the header.
	head = '1\n' + '1\n' + '1\n' + '0\n' + '1\t1\t1\n'
	if cuboid:
		head += '%s\t%s\t%s\n' % (bx, by, bz)
	else:
		head += '1\t1\t1\n'
	head += '%i\t%i\t%i\n' % (mref_lvl+3, leaf_nr, br_nr+leaf_nr)

	if cuboid:
		head += ('%1.8e\t' * (bx + 1)) % tuple(np.linspace(bounds[0][0], bounds[1][0], bx + 1)) + '\n'
		head += ('%1.8e\t' * (by + 1)) % tuple(np.linspace(bounds[0][1], bounds[1][1], by + 1)) + '\n'
		head += ('%1.8e\t' * (bz + 1)) % tuple(np.linspace(bounds[0][2], bounds[1][2], bz + 1)) + '\n'
	else:
		head += '%1.8e\t%1.8e\n' % (bounds[0][0], bounds[1][0])
		head += '%1.8e\t%1.8e\n' % (bounds[0][1], bounds[1][1])
		head += '%1.8e\t%1.8e\n' % (bounds[0][2], bounds[1][2])

	# Save the amr grid file with the corresponding header.
	writeout_file(fname='amr_grid', header=head, data=amr, dtype=ftype, ftype='inp')

	return leaf_nr, br_nr


# Creates a counter list for all the cells within a block.
# This is used to order the cells in an AMR manner (Z order).
def create_clist():
	import numpy as np

	# As each block contains 8**3 blocks we need a counter of the order of 4, one order higher than the number of cells.
	counter = np.zeros(4)

	# This represent the number of cells for each refinement level. With this we can compute the position of the lower
	# left corner of the 2x2x2 subblocks.
	cell_size = 2**(3 - np.linspace(0, 3, 3+1, dtype=int))

	clist = []
	# Each block can be split up in 64 2x2x2 subblocks.
	for i in range(64):
		# Compute the coordinates of the lower left corner for the case of 1x1x1 subblocks
		x = np.dot(counter % 2, cell_size)
		y = np.dot(np.floor(counter//2) % 2, cell_size)
		z = np.dot(np.floor(counter//4) % 2, cell_size)

		# Append to the counter list with a factor of 2 as the subblocks should cover 2x2x2 regions.
		clist.append(2 * np.asarray([z, y, x]).astype(int))

		# As all cells have the same refinement level only increase the highest ref. level.
		counter[3] += 1

		# Loop to check if we have reached 8 blocks in any refinement level.
		# If so we set it back to 0 and increase the next lower ref. level by one.
		while not np.all(counter != 8):
			for p in np.where(counter == 8)[0][::-1]:
				counter[p] -= 8
				counter[p-1] += 1

	clist = np.asarray(clist)
	return clist


def modify_block_data(bid, coords, block_size, xmin, xmax, data):
	import numpy as np

	d = data[bid]

	dhalf = block_size[bid] / 16.

	bxborders = np.linspace(
		coords[bid][0] - 0.5 * block_size[bid][0], coords[bid][0] + 0.5 * block_size[bid][0], 2 * 9 - 1,
		dtype=np.double
	)
	byborders = np.linspace(
		coords[bid][1] - 0.5 * block_size[bid][1], coords[bid][1] + 0.5 * block_size[bid][1], 2 * 9 - 1,
		dtype=np.double
	)
	bzborders = np.linspace(
		coords[bid][2] - 0.5 * block_size[bid][2], coords[bid][2] + 0.5 * block_size[bid][2], 2 * 9 - 1,
		dtype=np.double
	)

	dgridz, dgridy, dgridx = np.meshgrid(bzborders[1::2], byborders[1::2], bxborders[1::2])
	dgridx = dgridx.swapaxes(0, 1)
	dgridy = dgridy.swapaxes(0, 1)
	dgridz = dgridz.swapaxes(0, 1)

	grid = np.logical_or(
		np.logical_or(
			np.logical_or(dgridx + dhalf[0] < xmin[0], dgridx - dhalf[0] > xmax[0]),
			np.logical_or(dgridy + dhalf[1] < xmin[1], dgridy - dhalf[1] > xmax[1])),
		np.logical_or(dgridz + dhalf[2] < xmin[2], dgridz - dhalf[2] > xmax[2])
	)
	d[grid] = 1e-60
	return d


def writeout_dust(data, block_list, clist, leaf_nr, fname, block_size, coords, xmin, xmax, reduc=False):
	import numpy as np

	dd = []
	for bb in block_list:
		if reduc:
			d = modify_block_data(bid=bb, coords=coords, xmin=xmin, xmax=xmax, data=data, block_size=block_size)
		else:
			d = data[bb]
		for cli in clist:
			dd.append(d[cli[0]:cli[0] + 2, cli[1]:cli[1] + 2, cli[2]:cli[2] + 2])
	dd = np.asarray(dd, dtype=np.float64).flatten()
	dd[dd < 1e-50] = 1.e-60

	np.savetxt(X=dd, fname=fname, fmt='%1.8e', header='1\n%i\n1' % leaf_nr, comments='')


def writeout_dust_new(
		data, block_list, clist, leaf_nr, fname, block_size, coords, xmin, xmax, ftype, reduc=False, fend='inp'
):
	import numpy as np
	import os

	fpath, fname = os.path.split(fname)

	if fpath == '':
		fpath = '.'

	dd = []
	for bb in block_list:
		if reduc:
			d = modify_block_data(bid=bb, coords=coords, xmin=xmin, xmax=xmax, data=data, block_size=block_size)
		else:
			d = data[bb]
		for cli in clist:
			dd.append(d[cli[0]:cli[0] + 2, cli[1]:cli[1] + 2, cli[2]:cli[2] + 2])
	dd = np.asarray(dd, dtype=np.float64).flatten()
	dd[dd < 1e-50] = 1.e-60

	writeout_file(fname=fname, header='1\n%i\n1\n' % leaf_nr, data=dd, dtype=ftype, ftype=fend, fpath=fpath)


def writeout_gas(data, block_list, clist, leaf_nr, fname, block_size, coords, xmin, xmax, reduc=False):
	import numpy as np

	dd = []
	for bb in block_list:
		if reduc:
			d = modify_block_data(bid=bb, coords=coords, xmin=xmin, xmax=xmax, data=data, block_size=block_size)
		else:
			d = data[bb]
		for cli in clist:
			dd.append(d[cli[0]:cli[0] + 2, cli[1]:cli[1] + 2, cli[2]:cli[2] + 2])
	dd = np.asarray(dd, dtype=np.float64).flatten()
	dd[dd < 1e-50] = 1.e-60
	np.savetxt(X=dd, fname=fname, fmt='%1.8e', header='1\n%i' % leaf_nr, comments='')


def writeout_gas_new(
		data, block_list, clist, leaf_nr, fname, block_size, coords, xmin, xmax, ftype, reduc=False, fend='inp'
):
	import numpy as np
	import os

	fpath, fname = os.path.split(fname)

	if fpath == '':
		fpath = '.'

	dd = []
	for bb in block_list:
		if reduc:
			d = modify_block_data(bid=bb, coords=coords, xmin=xmin, xmax=xmax, data=data, block_size=block_size)
		else:
			d = data[bb]
		for cli in clist:
			dd.append(d[cli[0]:cli[0] + 2, cli[1]:cli[1] + 2, cli[2]:cli[2] + 2])
	dd = np.asarray(dd, dtype=np.float64).flatten()
	dd[dd < 1e-50] = 1.e-60

	writeout_file(fname=fname, header='1\n%i\n' % leaf_nr, data=dd, dtype=ftype, ftype=fend, fpath=fpath)


def writeout_vel_file(data, block_list, clist, leaf_nr, fname):
	import numpy as np

	ddx = []
	ddy = []
	ddz = []
	for bb in block_list:
		for cli in clist:
			ddx.append(data[0][bb][cli[0]:cli[0] + 2, cli[1]:cli[1] + 2, cli[2]:cli[2] + 2])
			ddy.append(data[1][bb][cli[0]:cli[0] + 2, cli[1]:cli[1] + 2, cli[2]:cli[2] + 2])
			ddz.append(data[2][bb][cli[0]:cli[0] + 2, cli[1]:cli[1] + 2, cli[2]:cli[2] + 2])
	ddx = np.asarray(ddx, dtype=np.float64).flatten()
	ddy = np.asarray(ddy, dtype=np.float64).flatten()
	ddz = np.asarray(ddz, dtype=np.float64).flatten()
	dd = np.transpose(np.asarray([ddx, ddy, ddz]))

	np.savetxt(X=dd, fname=fname, fmt='%1.8e\t%1.8e\t%1.8e', header='1\n%i' % leaf_nr, comments='')


def writeout_vel_file_new(data, block_list, clist, leaf_nr, fname, ftype, fend='inp'):
	import numpy as np
	import os

	fpath, fname = os.path.split(fname)

	if fpath == '':
		fpath = '.'

	ddx = []
	ddy = []
	ddz = []
	for bb in block_list:
		for cli in clist:
			ddx.append(data[0][bb][cli[0]:cli[0] + 2, cli[1]:cli[1] + 2, cli[2]:cli[2] + 2])
			ddy.append(data[1][bb][cli[0]:cli[0] + 2, cli[1]:cli[1] + 2, cli[2]:cli[2] + 2])
			ddz.append(data[2][bb][cli[0]:cli[0] + 2, cli[1]:cli[1] + 2, cli[2]:cli[2] + 2])
	ddx = np.asarray(ddx, dtype=np.float64).flatten()
	ddy = np.asarray(ddy, dtype=np.float64).flatten()
	ddz = np.asarray(ddz, dtype=np.float64).flatten()
	dd = np.asarray([ddx, ddy, ddz])

	writeout_file(fname=fname, header='1\n%i\n' % leaf_nr, data=dd, dtype=ftype, ftype=fend, fpath=fpath)


def flash2radmc(
		fname, dpath, xmin, xmax, chemnet, ftype, reduc=False, cuboid=False
):
	import numpy as np
	import scipy.constants as const
	import sys
	import os
	import h5py

	# Read in the FLASH file and create the 3d grid of the blocks.
	pf = h5py.File(name=fname, mode='r')

	# Change to folder where the data is supposed to be written to.
	cpath = os.getcwd()
	os.chdir(dpath)

	# Define some pointers for regular used data tables.
	coords = pf['coordinates'][()]
	gid = pf['gid'][()]
	refine_level = pf['refine level'][()]
	block_size = pf['block size'][()]

	# Reading the border of the simulation box.
	sim_dict = dict(pf['real runtime parameters'][()])

	simxl = sim_dict[('%-80s' % 'xmin').encode('UTF-8')]
	simxr = sim_dict[('%-80s' % 'xmax').encode('UTF-8')]
	simyl = sim_dict[('%-80s' % 'ymin').encode('UTF-8')]
	simyr = sim_dict[('%-80s' % 'ymax').encode('UTF-8')]
	simzl = sim_dict[('%-80s' % 'zmin').encode('UTF-8')]
	simzr = sim_dict[('%-80s' % 'zmax').encode('UTF-8')]

	xmin = np.asarray(xmin)
	xmax = np.asarray(xmax)

	print(xmin, xmax)

	# Set region to whole simulation box if region is not given.
	if xmin.size == 0 or xmax.size == 0:
		xmin = np.asarray([simxl, simyl, simzl])
		xmax = np.asarray([simxr, simyr, simzr])

	# Checking if the selected box is located within the simulation.
	if np.any(xmax <= np.asarray([simxl, simyl, simzl])) or np.any(xmin >= np.asarray([simxr, simyr, simzr])):
		sys.exit(
			'Selected box is not located within simulation. Please check selected coordinates.\n'
			'Simulation box:\tx\t%1.8e\t%1.8e\n' % (simxl, simxr) +
			'\t\ty\t%1.8e\t%1.8e\n' % (simyl, simyr) +
			'\t\tz\t%1.8e\t%1.8e\n\n' % (simzl, simzr) +
			'Selected box:\tx\t%1.8e\t%1.8e\n' % (xmin[0], xmax[0]) +
			'\t\ty\t%1.8e\t%1.8e\n' % (xmin[1], xmax[1]) +
			'\t\tz\t%1.8e\t%1.8e\n' % (xmin[2], xmax[2])
		)

	# Sanity check coordinates
	if np.any(xmax < xmin):
		sys.exit(
			'Check coordinates. Some value in xmin is larger than xmax.\n' +
			'xmin : %s\t%s\t%s\n' % tuple(xmin) +
			'xmax : %s\t%s\t%s\n' % tuple(xmax)
		)

	# Find all leaf blocks for inner and outer borders
	# Inner borders only includes blocks which center coordinate is included in selected region.
	# Outer borders also includes blocks which intersect with the selected region. Those variables are denoted with a 2.
	print('Finding all leaf blocks in given region')
	ind = np.argwhere(np.all(gid[:, 7:] == -1, axis=1)).T[0]
	tmpc = coords[ind]
	tmpbs = block_size[ind] * 0.5

	c = np.all(np.concatenate((tmpc >= xmin, tmpc <= xmax), axis=1), axis=1)
	c2 = np.all(np.concatenate((tmpc + tmpbs >= xmin, tmpc - tmpbs <= xmax), axis=1), axis=1)

	blist_raw = ind[c]
	blist_raw2 = ind[c2]

	if len(blist_raw) == 0:
		sys.exit(
			'No block center is availabe in the selected region.'
		)

	print(blist_raw.size, blist_raw2.size)
	bsmin = block_size[ind].min()

	brlvl = refine_level[blist_raw]
	brlvl2 = refine_level[blist_raw2]

	min_ref = brlvl.min()
	min_ref2 = brlvl2.min()

	max_ref_reg = brlvl.max()
	max_ref_reg2 = brlvl2.max()

	max_ref = refine_level[ind].max()
	print('Biggest block has refinement level: %s' % min_ref)
	print('Highest refinement level in simulation: %s' % max_ref)
	print('Highest refinement level in selected box: %s' % max_ref_reg)

	print('Extending region to fit amr structure at level %s.' % min_ref)
	blist_minref, bx, by, bz, bmax = find_blocks(
		block_list=blist_raw, min_ref_lvl=min_ref, max_ref_lvl=max_ref, block_size=block_size, brlvl=brlvl, bsmin=bsmin,
		coords=coords, gid=gid, refine_level=refine_level, center=(xmin+xmax)/2., cuboid=cuboid
	)

	while np.any([bx != bmax, by != bmax, bz != bmax]):
		print(
			'Trying extended region given by including blocks whose center is not in predefined region but part '
			'of their block volume is.')

		blist_minref, bx, by, bz, bmax = find_blocks(
			block_list=blist_raw2, min_ref_lvl=min_ref2, max_ref_lvl=max_ref, block_size=block_size, brlvl=brlvl2,
			bsmin=bsmin, coords=coords, gid=gid, refine_level=refine_level, center=(xmin + xmax) / 2., cuboid=cuboid
		)

		if cuboid and type(blist_minref) != int:
			min_ref = min_ref2
			brlvl = brlvl2
			blist_raw = blist_raw2
			max_ref_reg = max_ref_reg2
			break

		if bx == bmax and by == bmax and bz == bmax:
			min_ref = min_ref2
			brlvl = brlvl2
			blist_raw = blist_raw2
			max_ref_reg = max_ref_reg2
			print('New biggest block has refinement level: %s' % min_ref)
			print('New highest refinement level in simulation: %s' % max_ref)
			print('New highest refinement level in selected box: %s' % max_ref_reg)

		else:
			min_ref -= 1
			min_ref2 -= 1
			if min_ref == 0:
				sys.exit('Region could not be extended to a cubic block shape.')

			print('Could not extend region far enough. Extending again at level %s' % min_ref)
			blist_minref, bx, by, bz, bmax = find_blocks(
				block_list=blist_raw, min_ref_lvl=min_ref, max_ref_lvl=max_ref, block_size=block_size, brlvl=brlvl,
				bsmin=bsmin, coords=coords, gid=gid, refine_level=refine_level, center=(xmin+xmax)/2., cuboid=cuboid
			)

			if cuboid and type(blist_minref) != int:
				break

	if cuboid:
		blvl = min_ref
	else:
		blvl = min_ref - int(np.round(np.log2(bmax)))

	blist_maxref, b_tot_nr = create_blists(
		minref_blist=blist_minref, block_level=blvl, gid=gid, coords=coords, max_ref_lvl=max_ref,
		bnx=bx, bny=by, bnz=bz, cuboid=cuboid
	)

	if not cuboid:
		b_tot_nr += np.sum(np.logspace(start=0., stop=np.log2(bmax)-1, base=2, num=int(np.log2(bmax)))**3).astype(int)
		# print(b_tot_nr)

	# b_tot_nr = get_max_block_nr(
	# 	block_list=blist_maxref, max_ref_lvl=max_ref, block_lvl=blvl, refine_level=refine_level
	# )

	print('Total number of blocks including parent blocks: %s' % b_tot_nr)

	counter_list = create_clist()

	boundariesl = np.min(coords[blist_maxref] - 0.5 * block_size[blist_maxref], axis=0)
	boundariesr = np.max(coords[blist_maxref] + 0.5 * block_size[blist_maxref], axis=0)
	print('Region extended to:\tlower\t\tupper')
	print('\tx\t%+1.8e\t\t%+1.8e' % (boundariesl[0], boundariesr[0]))
	print('\ty\t%+1.8e\t\t%+1.8e' % (boundariesl[1], boundariesr[1]))
	print('\tz\t%+1.8e\t\t%+1.8e\n' % (boundariesl[2], boundariesr[2]))

	print('Region center at:\t%+1.8e\t%+1.8e\t%+1.8e' % tuple((boundariesl+boundariesr)/2))
	print('Given center:\t\t%+1.8e\t%+1.8e\t%+1.8e' % tuple((xmin+xmax)/2))

	print('Filling up tree structure with block ids.')
	bid_list = create_full_tree(
		block_list=blist_maxref, tot_nr_blocks=b_tot_nr, max_ref_lvl=max_ref, refine_level=refine_level,
		min_ref_lvl=min_ref, cuboid=cuboid
	)

	print('Determine refinement structure for all blocks.')
	bid_ref = fill_refine_level(
		block_list=bid_list, max_ref_lvl=max_ref, block_lvl=blvl, refine_level=refine_level, cuboid=cuboid
	)

	print('Building gid tree.')
	bid_gid = build_gid(
		ref_lvl=bid_ref, tot_nr_blocks=b_tot_nr, max_ref_lvl=max_ref, block_level=blvl, cuboid=cuboid
	)

	print('Building amr grid and writing out amr_grid.inp')
	leafs, blocks = writeout_amrgrid(
		gid_list=bid_gid, bounds=[boundariesl, boundariesr], mref_lvl=max_ref_reg-blvl, ftype=ftype,
		bx=int(bx), by=int(by), bz=int(bz), cuboid=cuboid
	)

	# Define some constants for writing out the temperature and densities.
	au = const.atomic_mass/const.gram
	mu_h = 1.00
	mu_h2 = 2 * mu_h
	mu_c = 12.0
	mu_co = 28.
	mu_o = 15.9994
	mu_he = 4.002602
	nr_x = 1
	mu_m = 28
	# k = const.k/const.erg
	# mu = 2.3

	# Normalization factor depends on the chemical network. Should be 1 in most cases besides NL97
	normalization = 1.0

	if chemnet == 'NL97':
		# See Annika Franeck Doctor Thesis page 76 for nomalization factor.
		ab_he = 0.1
		ab_c = 1.4e-4
		ab_o = 3.2e-4
		mu_he = 4.002602
		mu_c = 12.011
		mu_o = 15.9994
		normalization = 1. + ab_he * mu_he + ab_c * mu_c + ab_o * mu_o
		mu_co = 12.

	elif chemnet == 'NL99':
		normalization = 1.0
		mu_co = 28.

	# Writing out all fields if they are availabe.
	# Maybe introduce a selection criterium to only write out necessary files for the given RADMC simulation.
	if 'dens' in pf.keys():
		print('Writing out gas_density')
		writeout_gas_new(
			data=pf['dens'][()], fname='gas_density', block_list=blist_maxref, leaf_nr=leafs,
			clist=counter_list, block_size=block_size, coords=coords, xmin=xmin, xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'iha ' in pf.keys():
		print('Writing out numberdens_ha')
		writeout_gas_new(
			data=pf['dens'][()] * pf['iha '][()] / (normalization * au * mu_h), fname='numberdens_ha',
			block_list=blist_maxref, clist=counter_list, leaf_nr=leafs, block_size=block_size, coords=coords, xmin=xmin,
			xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'ih2 ' in pf.keys():
		print('Writing out numberdens_h2')
		writeout_gas_new(
			data=pf['dens'][()] * pf['ih2 '][()] / (normalization * au * mu_h2), fname='numberdens_h2',
			block_list=blist_maxref, clist=counter_list, leaf_nr=leafs, block_size=block_size, coords=coords, xmin=xmin,
			xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'ihp ' in pf.keys():
		print('Writing out numberdens_hp')
		writeout_gas_new(
			data=pf['dens'][()] * pf['ihp '][()] / (normalization * au * mu_h), fname='numberdens_hp',
			block_list=blist_maxref, clist=counter_list, leaf_nr=leafs, block_size=block_size, coords=coords, xmin=xmin,
			xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'ico ' in pf.keys():
		print('Writing out numberdens_co')
		writeout_gas_new(
			data=pf['dens'][()] * pf['ico '][()] / (normalization * au * mu_co), fname='numberdens_co',
			block_list=blist_maxref, clist=counter_list, leaf_nr=leafs, block_size=block_size, coords=coords, xmin=xmin,
			xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'ic  ' in pf.keys():
		print('Writing out numberdens_c')
		writeout_gas_new(
			data=pf['dens'][()] * pf['ic  '][()] / (normalization * au * mu_c), fname='numberdens_c',
			block_list=blist_maxref, clist=counter_list, leaf_nr=leafs, block_size=block_size, coords=coords, xmin=xmin,
			xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'ichx' in pf.keys():
		print('Writing out numberdens_chx')
		writeout_gas_new(
			data=pf['dens'][()] * pf['ichx'][()] / (normalization * au * (mu_c + mu_h * nr_x)),
			fname='numberdens_chx', block_list=blist_maxref, clist=counter_list, leaf_nr=leafs,
			block_size=block_size, coords=coords, xmin=xmin, xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'iohx' in pf.keys():
		print('Writing out numberdens_ohx')
		writeout_gas_new(
			data=pf['dens'][()] * pf['iohx'][()] / (normalization * au * (mu_c + mu_h * nr_x)),
			fname='numberdens_ohx', block_list=blist_maxref, clist=counter_list, leaf_nr=leafs,
			block_size=block_size, coords=coords, xmin=xmin, xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'ihco' in pf.keys():
		print('Writing out numberdens_hco')
		writeout_gas_new(
			data=pf['dens'][()] * pf['ihco'][()] / (normalization * au * (mu_h + mu_c + mu_o)),
			fname='numberdens_hco',	block_list=blist_maxref, clist=counter_list, leaf_nr=leafs,
			block_size=block_size, coords=coords, xmin=xmin, xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'ihe ' in pf.keys():
		print('Writing out numberdens_he')
		writeout_gas_new(
			data=pf['dens'][()] * pf['ihe '][()] / (normalization * au * mu_he), fname='numberdens_he',
			block_list=blist_maxref, clist=counter_list, leaf_nr=leafs, block_size=block_size, coords=coords, xmin=xmin,
			xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'ihep' in pf.keys():
		print('Writing out numberdens_hep')
		writeout_gas_new(
			data=pf['dens'][()] * pf['ihep'][()] / (normalization * au * mu_he), fname='numberdens_hep',
			block_list=blist_maxref, clist=counter_list, leaf_nr=leafs, block_size=block_size, coords=coords, xmin=xmin,
			xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'im  ' in pf.keys():
		print('Writing out numberdens_m')
		writeout_gas_new(
			data=pf['dens'][()] * pf['im  '][()] / (normalization * au * mu_m), fname='numberdens_m',
			block_list=blist_maxref, clist=counter_list, leaf_nr=leafs, block_size=block_size, coords=coords, xmin=xmin,
			xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'imp ' in pf.keys():
		print('Writing out numberdens_mp')
		writeout_gas_new(
			data=pf['dens'][()] * pf['imp '][()] / (normalization * au * mu_m), fname='numberdens_mp',
			block_list=blist_maxref, clist=counter_list, leaf_nr=leafs, block_size=block_size, coords=coords, xmin=xmin,
			xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'io  ' in pf.keys():
		print('Writing out numberdens_o')
		writeout_gas_new(
			data=pf['dens'][()] * pf['io  '][()] / (normalization * au * mu_o), fname='numberdens_o',
			block_list=blist_maxref, clist=counter_list, leaf_nr=leafs, block_size=block_size, coords=coords, xmin=xmin,
			xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'icp ' in pf.keys():
		print('Writing out numberdens_cp')
		writeout_gas_new(
			data=pf['dens'][()] * pf['icp '][()] / (normalization * au * mu_c), fname='numberdens_cp',
			block_list=blist_maxref, clist=counter_list, leaf_nr=leafs, block_size=block_size, coords=coords, xmin=xmin,
			xmax=xmax, reduc=reduc, ftype=ftype
		)
	if 'temp' in pf.keys():
		print('Writing out gas_temperature')
		writeout_gas_new(
			data=pf['temp'][()], fname='gas_temperature', block_list=blist_maxref, clist=counter_list,
			leaf_nr=leafs, block_size=block_size, coords=coords, xmin=xmin, xmax=xmax, reduc=False, ftype=ftype
		)
	if 'tdus' in pf.keys():
		print('Writing out dust_temperature')
		writeout_dust_new(
			data=pf['tdus'][()], fname='dust_temperature', block_list=blist_maxref, clist=counter_list,
			leaf_nr=leafs, block_size=block_size, coords=coords, xmin=xmin, xmax=xmax, reduc=False, ftype=ftype,
			fend='dat'
		)
	if 'velx' in pf.keys():
		print('Writing out gas_velocity')
		writeout_vel_file_new(
			data=[pf['velx'][()], pf['vely'][()], pf['velz'][()]], fname='gas_velocity',
			block_list=blist_maxref, clist=counter_list, leaf_nr=leafs, ftype=ftype
		)

	os.chdir(cpath)


# ****************** sph routine ************************* #

# Work in progress.

# ****************** create data ************************ #


def create_data(
		fname, dpath, script_folder, dtype, mol_type, chemnet, radset, linesset,
		cameraset, coords=None, threshold=20000.0, freezeout=0, crir=3.e-17, wf=1,
		dust_to_gas=0.01, mol_mass=2.3, debug=False, rewrite=False, reduc=False,
		ftype='ascii', cuboid=False
):
	import os
	import sys
	import shutil
	import numpy as np

	curr_path = os.getcwd()
	os.chdir(dpath)
	sys.path.insert(0, script_folder)

	if '_' in mol_type:
		tmp = np.asarray(mol_type.split('_'))
		flag_all = True
		mol_type = tmp[tmp != 'all'][0]
	else:
		flag_all = False

	all_list = dict({
		'dust_dens'		: False,
		'microturb'		: False,
		'para_ortho'	: False,
		'cp_e'			: False,
		'co'			: False,
		'extra_species'	: False,
		'13cp'			: False,
		'spintemp'		: False
	})

	if debug:
		check_dependencies(script_folder=script_folder)

	if not rewrite:
		if dtype == 'octtree':
			silcc2radmc(fname=fname, dpath=dpath, coords=coords, chemnet=chemnet, ftype=ftype)
		elif dtype == 'hdf5':
			yt2radmc(fname=fname, dpath=dpath, chemnet=chemnet)
		elif dtype == 'flash':
			if len(coords) == 6:
				xmin = coords[0:3]
				xmax = coords[3:6]
			else:
				xmin = []
				xmax = []
			flash2radmc(
				fname=fname, dpath=dpath, xmin=xmin, xmax=xmax, reduc=reduc, chemnet=chemnet, ftype=ftype,
				cuboid=cuboid
			)
		else:
			sys.exit('Set data type to either hdf5, flash or octtree.')

		if mol_type == 'co' or mol_type == '13co' or mol_type == 'c18o':
			create_dust_dens(fpath=dpath, dust_to_gas=dust_to_gas, ftype=ftype)
			all_list['dust_dens'] = True

			create_microturbulence_mol(fpath=dpath, mol_mass=mol_mass, ftype=ftype)
			all_list['microturb'] = True

			create_para_ortho(fpath=dpath, ftype=ftype)
			all_list['para_ortho'] = True

			create_cp_e(fpath=dpath, script_folder=script_folder, threshold=threshold, ftype=ftype)
			all_list['cp_e'] = True

			if chemnet == 'NL99':
				create_extra_species(fpath=dpath, ftype=ftype)
				all_list['extra_species'] = True
			elif chemnet == 'NL97':
				create_he(fpath=dpath, ftype=ftype)

			create_co(fpath=dpath, freezeout=freezeout, crir=crir, ftype=ftype)
			all_list['co'] = True

			create_radmc_inp(settings=radset)
			create_lines_inp(settings=linesset)
			shutil.copy2(os.path.join(script_folder, 'molecule_data', 'molecule_%s.inp' % mol_type), dpath)
			add_high_temp(fpath='molecule_%s.inp' % mol_type, dpath=dpath)

		if mol_type == '13cp' or mol_type == 'cp' or mol_type == 'cpuv':
			create_dust_dens(fpath=dpath, dust_to_gas=dust_to_gas, ftype=ftype)
			all_list['dust_dens'] = True

			create_microturbulence_mol(fpath=dpath, mol_mass=mol_mass, ftype=ftype)
			all_list['microturb'] = True

			create_para_ortho(fpath=dpath, ftype=ftype)
			all_list['para_ortho'] = True

			create_cp_e(fpath=dpath, script_folder=script_folder, threshold=threshold, ftype=ftype)
			all_list['cp_e'] = True

			if chemnet == 'NL99':
				create_extra_species(fpath=dpath, ftype=ftype)
				all_list['extra_species'] = True
			elif chemnet == 'NL97':
				create_he(fpath=dpath, ftype=ftype)

			convert_13cp(fpath=dpath, ftype=ftype)
			all_list['13cp'] = True

			create_radmc_inp(settings=radset)
			create_lines_inp(settings=linesset)
			shutil.copy2(os.path.join(script_folder, 'molecule_data', 'molecule_%s.inp' % mol_type), dpath)
			add_high_temp(fpath='molecule_%s.inp' % mol_type, dpath=dpath)

		if mol_type == 'ha':
			create_dust_dens(fpath=dpath, dust_to_gas=dust_to_gas, ftype=ftype)
			all_list['dust_dens'] = True

			create_microturbulence_mol(fpath=dpath, mol_mass=mol_mass, ftype=ftype)
			all_list['microturb'] = True

			create_para_ortho(fpath=dpath, ftype=ftype)
			all_list['para_ortho'] = True

			create_cp_e(fpath=dpath, script_folder=script_folder, threshold=threshold, ftype=ftype)
			all_list['cp_e'] = True

			if chemnet == 'NL99':
				create_extra_species(fpath=dpath, ftype=ftype)
				all_list['extra_species'] = True
			elif chemnet == 'NL97':
				create_he(fpath=dpath, ftype=ftype)

			create_spintemp_wf(fpath=dpath, wf=wf, ftype=ftype)
			all_list['spintemp'] = True

			linesset = ['2', '1', 'ha    leiden    0    0    0']
			create_radmc_inp(settings=radset)
			create_lines_inp(settings=linesset)
			shutil.copy2(os.path.join(script_folder, 'molecule_data', 'molecule_%s.inp' % mol_type), dpath)

		if mol_type == 'dust':
			create_dust_dens(fpath=dpath, dust_to_gas=dust_to_gas, ftype=ftype)
			all_list['dust_dens'] = True

			shutil.copy2(os.path.join(script_folder, 'dustopac.inp'), dpath)
			shutil.copy2(os.path.join(script_folder, 'dustkappa_silicate.inp'), dpath)

		if flag_all:
			if not all_list['dust_dens']:
				create_dust_dens(fpath=dpath, dust_to_gas=dust_to_gas, ftype=ftype)

			if not all_list['microturb']:
				create_microturbulence_mol(fpath=dpath, mol_mass=mol_mass, ftype=ftype)

			if not all_list['para_ortho']:
				create_para_ortho(fpath=dpath, ftype=ftype)

			if not all_list['cp_e']:
				create_cp_e(fpath=dpath, script_folder=script_folder, threshold=threshold, ftype=ftype)

			if chemnet == 'NL99':
				if not all_list['extra_species']:
					create_extra_species(fpath=dpath, ftype=ftype)
			elif chemnet == 'NL97':
				create_he(fpath=dpath, ftype=ftype)

			if not all_list['co']:
				create_co(fpath=dpath, freezeout=freezeout, crir=crir, ftype=ftype)

			if not all_list['13cp']:
				convert_13cp(fpath=dpath, ftype=ftype)

			if not all_list['spintemp']:
				create_spintemp_wf(fpath=dpath, wf=wf, ftype=ftype)

		shutil.copy2(os.path.join(script_folder, 'wavelength_micron.inp'), dpath)
		make_wavelength_file(lam=cameraset['lam_0'], delv=cameraset['widthkms'], nlam=cameraset['nlam'])

	else:
		create_radmc_inp(settings=radset)
		create_lines_inp(settings=linesset)
		if mol_type != 'dust':
			shutil.copy2(os.path.join(script_folder, 'molecule_data', 'molecule_%s.inp' % mol_type), dpath)
			add_high_temp(fpath='molecule_%s.inp' % mol_type, dpath=dpath)
			shutil.copy2(os.path.join(script_folder, 'wavelength_micron.inp'), dpath)
			make_wavelength_file(lam=cameraset['lam_0'], delv=cameraset['widthkms'], nlam=cameraset['nlam'])

	os.chdir(curr_path)
